package com.example.taeconsultant.finalproject.ui.detailfragment;

import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.example.taeconsultant.finalproject.data.network.models.VenuesModel;
import com.example.taeconsultant.finalproject.ui.base.IMVPView;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public interface IDetailMVPView extends IMVPView {

    void onFetchDataCompleted(EventsModel.Embedded.Event event);

    void onFetchDataCompleted(VenuesModel.Embedded.Venue venue);
}
