package com.example.taeconsultant.finalproject.di.component;

import com.example.taeconsultant.finalproject.DetailFragment;
import com.example.taeconsultant.finalproject.ListFragment;
import com.example.taeconsultant.finalproject.TagCloudActivity;
import com.example.taeconsultant.finalproject.di.module.CloudActivityModule;
import com.example.taeconsultant.finalproject.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by samuelpatmore on 31/01/2018.
 */


@PerActivity
@Component(modules = CloudActivityModule.class)
public interface ICloudActivityComponent {

    void inject(TagCloudActivity activity);
}
