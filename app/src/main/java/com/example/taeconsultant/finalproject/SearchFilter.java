package com.example.taeconsultant.finalproject;

/**
 * Created by TAE Consultant on 19/10/2017.
 */

public class SearchFilter {
    private String
            keyword,
            latlong,
            unit,
            sort,
            countryCode,
            classificationId,
            includeLicensedContent;

    private int
            radius,
            size,
            page;

    public SearchFilter() {
    }

    /**
     *
     * @param keyword
     * @param latlong
     * @param unit
     * @param sort
     * @param countryCode
     * @param classificationId
     * @param includeLicensedContent
     * @param radius
     * @param size
     * @param page
     */
    public SearchFilter(String keyword, String latlong, String unit, String sort, String countryCode, String classificationId, String includeLicensedContent, int radius, int size, int page) {
        this.keyword = keyword;
        this.latlong = latlong;
        this.unit = unit;
        this.sort = sort;
        this.countryCode = countryCode;
        this.classificationId = classificationId;
        this.includeLicensedContent = includeLicensedContent;
        this.radius = radius;
        this.size = size;
        this.page = page;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getClassificationId() {
        return classificationId;
    }

    public void setClassificationId(String classificationId) {
        this.classificationId = classificationId;
    }

    public String getIncludeLicensedContent() {
        return includeLicensedContent;
    }

    public void setIncludeLicensedContent(String includeLicensedContent) {
        this.includeLicensedContent = includeLicensedContent;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
