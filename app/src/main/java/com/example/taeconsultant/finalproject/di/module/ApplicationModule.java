package com.example.taeconsultant.finalproject.di.module;

import android.app.Application;
import android.content.Context;

import com.example.taeconsultant.finalproject.data.AppDataManager;
import com.example.taeconsultant.finalproject.data.IDataManager;
import com.example.taeconsultant.finalproject.data.network.AppAPIHelper;
import com.example.taeconsultant.finalproject.data.network.IAPIHelper;
import com.example.taeconsultant.finalproject.di.scope.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by TAE Consultant on 03/10/2017.
 */

@Module
public class ApplicationModule {

    Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @ApplicationContext
    Context context() {
        return application;
    }

    @Provides
    Application getApplication() {
        return application;
    }

    @Provides
    @Singleton
    IDataManager provideAppDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    IAPIHelper provideAppAPIHelper(AppAPIHelper appAPIHelper) {
        return appAPIHelper;
    }
}
