package com.example.taeconsultant.finalproject.ui.detailfragment;

import com.example.taeconsultant.finalproject.ui.base.IMVPPresenter;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public interface IDetailMVPPresenter<V extends IDetailMVPView> extends IMVPPresenter<V> {

    void onEventDetailViewPrepared(String id);

    void onVenueDetailViewPrepared(String id);
}
