package com.example.taeconsultant.finalproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.example.taeconsultant.finalproject.adapters.TextTagsAdapter;
import com.example.taeconsultant.finalproject.data.AppDataManager;
import com.example.taeconsultant.finalproject.data.IDataManager;
import com.example.taeconsultant.finalproject.data.network.models.ClassificationsModel;
import com.example.taeconsultant.finalproject.di.component.DaggerICloudActivityComponent;
import com.example.taeconsultant.finalproject.di.component.ICloudActivityComponent;
import com.example.taeconsultant.finalproject.di.module.CloudActivityModule;
import com.example.taeconsultant.finalproject.utility.Constants;
import com.example.taeconsultant.finalproject.utility.rx.AppSchedulerProvider;
import com.example.taeconsultant.finalproject.utility.rx.SchedulerProvider;
import com.moxun.tagcloudlib.view.TagCloudView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.CompositeDisposable;

public class TagCloudActivity extends AppCompatActivity {

    private static final String MUSIC = "Music", FILMS = "Films", SPORTS = "Sports", ARTS = "Arts & Theatre";

    ICloudActivityComponent mICloudActivityComponent;

    @Inject CompositeDisposable mCompositeDisposable;

    @Inject SchedulerProvider mSchedulerProvider;

    @Inject IDataManager mDataManager;

    @BindView(R.id.tag_cloud)
    TagCloudView tagCloudView;

    @BindView(R.id.backgroundImage)
    ImageView mImageView;

    private TextTagsAdapter textTagsAdapter;

    private void initializeDagger() {
        mICloudActivityComponent = DaggerICloudActivityComponent.builder()
                .cloudActivityModule(new CloudActivityModule(this))
                .build();

        mICloudActivityComponent.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_cloud);
        ButterKnife.bind(this);
        initializeDagger();

        textTagsAdapter = new TextTagsAdapter(new ArrayList<Classification>());
        tagCloudView.setAdapter(textTagsAdapter);

        populateClassificationsFromServer();
    }

    private void populateClassificationsFromServer() {
        getCompositeDisposable()
                .add(getDataManager().useCaseClassifications(Constants.API_KEY)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(this::unpackClassifications,
                                throwable -> {
                                    throwable.printStackTrace();
                                }
                        )
                );
    }

    private void unpackClassifications(ClassificationsModel classificationsModel) {
        List<Classification> classifications = new ArrayList<>();

        for(ClassificationsModel.Embedded.Classification classification : classificationsModel.getEmbedded().getClassifications()) {
            if (classification.getSegment() != null
                    && classification.getSegment().getName() != null
                    && classification.getSegment().getId() != null) {

                classifications.add(new Classification(classification.getSegment().getName(), classification.getSegment().getName(), classification.getSegment().getId()));

                for (int j = 0; j < Math.min(classification.getSegment().getEmbedded().getGenres().size(), 5); j++) {

                    Classification newClassification = new Classification(classification.getSegment().getName(),
                            classification.getSegment().getEmbedded().getGenres().get(j).getName(),
                            classification.getSegment().getEmbedded().getGenres().get(j).getId());
                    classifications.add(newClassification);
                }
            }
        }
        for(Classification c : classifications) {
            Log.d("Classification:",  c.segment + ":" + c.genre + " ; " + c.id);
        }
        textTagsAdapter = new TextTagsAdapter(classifications);
        tagCloudView.setAdapter(textTagsAdapter);
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public IDataManager getDataManager() {
        return mDataManager;
    }
}
