package com.example.taeconsultant.finalproject;

/**
 * Created by samuelpatmore on 29/01/2018.
 */

public class Classification {

    public final String genre, segment, id;

    public Classification(String segment, String genre, String id) {
        this.genre = genre;
        this.segment = segment;
        this.id = id;
    }
}
