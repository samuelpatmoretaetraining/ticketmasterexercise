package com.example.taeconsultant.finalproject.data;

import com.example.taeconsultant.finalproject.data.network.AppAPIHelper;
import com.example.taeconsultant.finalproject.data.network.IAPIHelper;
import com.example.taeconsultant.finalproject.data.network.models.ClassificationsModel;
import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.example.taeconsultant.finalproject.data.network.models.VenuesModel;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public class AppDataManager implements IDataManager{

    private IAPIHelper iapiHelper;

    @Inject
    public AppDataManager() {
        this.iapiHelper = new AppAPIHelper();
    }

    @Override
    public Observable<EventsModel> useCaseEvents(
            String apikey,
            String keyword,
            String latlong,
            int radius,
            String unit,
            int size,
            int page,
            String sort,
            String countryCode,
            String classificationId,
            String includeLicensedContent) {
        return iapiHelper.useCaseEvents(apikey, keyword, latlong, radius, unit, size, page, sort, countryCode, classificationId, includeLicensedContent);
    }

    @Override
    public Observable<EventsModel.Embedded.Event> useCaseEventDetails(String id, String apikey) {
        return iapiHelper.useCaseEventDetails(id, apikey);
    }

    @Override
    public Observable<ClassificationsModel> useCaseClassifications(String apikey) {
        return iapiHelper.useCaseClassifications(apikey);
    }

    @Override
    public Observable<VenuesModel.Embedded.Venue> useCaseVenueDetails(String id, String apikey) {
        return iapiHelper.useCaseVenueDetails(id, apikey);
    }
}
