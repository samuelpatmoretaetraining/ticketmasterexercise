package com.example.taeconsultant.finalproject.di.component;

import android.app.Application;

import com.example.taeconsultant.finalproject.MyApplication;
import com.example.taeconsultant.finalproject.data.IDataManager;
import com.example.taeconsultant.finalproject.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by TAE Consultant on 03/10/2017.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface IApplicationComponent {

    void inject(MyApplication myApplication);

    Application getApplication();

    IDataManager getDataManager();
}
