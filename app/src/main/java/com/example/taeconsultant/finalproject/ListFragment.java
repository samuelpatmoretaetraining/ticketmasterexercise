package com.example.taeconsultant.finalproject;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.example.taeconsultant.finalproject.adapters.ListAdapter;
import com.example.taeconsultant.finalproject.data.database.controller.RealmController;
import com.example.taeconsultant.finalproject.data.database.localdb.EventsDatabase;
import com.example.taeconsultant.finalproject.data.network.AppAPIHelper;
import com.example.taeconsultant.finalproject.data.network.models.ClassificationsModel;
import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.example.taeconsultant.finalproject.di.component.DaggerIActivityComponent;
import com.example.taeconsultant.finalproject.di.component.IActivityComponent;
import com.example.taeconsultant.finalproject.di.module.ActivityModule;
import com.example.taeconsultant.finalproject.ui.base.BaseFragment;
import com.example.taeconsultant.finalproject.ui.listfragment.IListMVPView;
import com.example.taeconsultant.finalproject.ui.listfragment.ListPresenter;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;

import static com.example.taeconsultant.finalproject.MyApplication.getApplication;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends BaseFragment implements IListMVPView {

    Realm realm;
    RealmController realmController;
    EventsDatabase eventsDatabase;

    IActivityComponent mIActivityComponent;

    @Inject
    ListPresenter<IListMVPView> listMVPViewListPresenter;

    List<String> countryCodes = Arrays.asList("",
            "US", "GB", "AD", "AI", "AR", "AU", "AT", "AZ", "BS",
            "BH", "BE", "BM", "BR", "BG", "CA", "CL", "CN", "CO",
            "CR", "HR", "CY", "CZ", "DK", "DO", "EC", "EE", "FO",
            "FI", "FR", "GE", "DE", "GH", "GI", "GR", "HK", "HU",
            "IS", "IN", "IE", "IL", "IT", "JM", "JP", "KR", "LV",
            "LB", "LT", "LU", "MY", "MT", "MX", "MC", "ME", "MA",
            "NL", "AN", "NZ", "ND", "NO", "PE", "PT", "RO", "RU",
            "LC", "SA", "RS", "SG", "SK", "SI", "ZA", "ES", "SE",
            "CH", "TW", "TH", "TT", "TR", "UA", "AE", "UY", "VE");

    List<String> classificationIDs = new ArrayList<>();

    List<String> classifications = new ArrayList<>();

    SearchFilter searchFilter;

    @BindView(R.id.cvNoResults)
    CardView cvNoResults;
    @BindView(R.id.elFilters)
    ExpandableLayout elFilters;
    @BindView(R.id.rvList)
    ShimmerRecyclerView rvList;
    @BindView(R.id.tvRadiusConcat)
    TextView tvRadiusConcat;
    @BindView(R.id.btnAdvanced)
    Button btnAdvanced;
    @BindView(R.id.btnApply)
    Button btnApply;
    @BindView(R.id.checkLocation)
    CheckBox checkLocation;
    @BindView(R.id.etKeyword)
    EditText etKeyword;
    @BindView(R.id.spnrCountries)
    Spinner spnrCountries;
    @BindView(R.id.spnrClassifications)
    Spinner spnrClassifications;
    @BindView(R.id.spnrSort)
    Spinner spnrSort;
    @BindView(R.id.seekRadius)
    SeekBar seekRadius;
    @BindView(R.id.swtchUnit)
    Switch swtchUnit;

    public ListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeRealm();

        initializeViews(view);

        initializeDagger();

        listMVPViewListPresenter.onAttach(this);
        listMVPViewListPresenter.onClassificationsViewPrepared();
    }

    private void initializeRealm() {
        realm = Realm.getDefaultInstance();
        realmController = new RealmController(realm);
    }

    private void initializeDagger() {
        mIActivityComponent = DaggerIActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .iApplicationComponent(((MyApplication) getApplication()).getiApplicationComponent())
                .build();

        getIFragmentComponent().inject(this);
    }

    public void initializeViews(View view) {

        rvList.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        rvList.showShimmerAdapter();

        tvRadiusConcat.setText("10 " + getString(R.string.radius_unit_km));

        btnAdvanced.setOnClickListener(view12 -> {
            if (elFilters.isExpanded()) {
                elFilters.collapse(true);
            } else {
                elFilters.expand(true);
            }
        });

        btnApply.setOnClickListener(view1 -> {
            cvNoResults.setVisibility(View.INVISIBLE);
            elFilters.collapse(true);
            rvList.showShimmerAdapter();
            rvList.setVisibility(View.VISIBLE);

            if (((MainActivity) getActivity()).userLocation != null) {
                String latlong = ((MainActivity) getActivity()).userLocation.getLatitude() + "," + ((MainActivity) getActivity()).userLocation.getLongitude();
                searchFilter = new SearchFilter(
                        etKeyword.getText().toString(),
                        latlong,
                        tvRadiusConcat.getText().toString().split(" ")[1],
                        AppAPIHelper.SortParam.values()[spnrSort.getSelectedItemPosition()].toString(),
                        "",
                        classificationIDs.get(spnrClassifications.getSelectedItemPosition() != -1 ? spnrClassifications.getSelectedItemPosition() : 0), // use 0 instead of -1 to avoid ArrayOutOfBoundsException
                        "yes",
                        seekRadius.getProgress(),
                        100,
                        0);
            } else {
                searchFilter = new SearchFilter(
                        etKeyword.getText().toString(),
                        "",
                        tvRadiusConcat.getText().toString().split(" ")[1],
                        AppAPIHelper.SortParam.values()[spnrSort.getSelectedItemPosition()].toString(),
                        countryCodes.get(spnrCountries.getSelectedItemPosition()),
                        classificationIDs.get(spnrClassifications.getSelectedItemPosition()),
                        "yes",
                        seekRadius.getProgress(),
                        100,
                        0);
            }

            Log.d("Keyword", searchFilter.getKeyword());
            Log.d("ClassificationId", searchFilter.getClassificationId());

            listMVPViewListPresenter.onEventsViewPrepared(
                    searchFilter.getKeyword(),
                    searchFilter.getLatlong(),
                    searchFilter.getRadius(),
                    searchFilter.getUnit(),
                    searchFilter.getSize(),
                    searchFilter.getPage(),
                    searchFilter.getSort(),
                    searchFilter.getCountryCode(),
                    searchFilter.getClassificationId(),
                    searchFilter.getIncludeLicensedContent());
        });

        checkLocation.setOnCheckedChangeListener((compoundButton, b) -> spnrCountries.setEnabled(!b));

        spnrCountries.setEnabled(false);
        spnrSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        seekRadius.setProgress(10);
        seekRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tvRadiusConcat.setText(((i < 100) ? i + " " : "∞ ") + tvRadiusConcat.getText().toString().split(" ")[1]);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        swtchUnit.setOnCheckedChangeListener((compoundButton, b) -> tvRadiusConcat.setText(tvRadiusConcat.getText().toString().split(" ")[0] + " " + ((b) ? swtchUnit.getTextOn() : swtchUnit.getTextOff())));
    }

    @Override
    public void onFetchDataCompleted(final EventsModel eventsModel) {
        if (eventsModel.getPage().getTotalElements() > 0) {
            Log.i("FINALPROJECT","ListFragment: onFetchDataCompleted(EventsModel)");

            realmController.deleteDatabase();
            for (EventsModel.Embedded.Event event : eventsModel.getEmbedded().getEvents()) {

                //TODO: GET IMAGES WORKING IN REALM
                ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                ByteArrayOutputStream stream2 = new ByteArrayOutputStream();

//                Picasso.with(getContext())
//                        .load(event.getImages().get(0).getUrl())
//                        .resize(100, 100)
//                        .centerCrop()
//                        .into(new Target() {
//                            @Override
//                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream1);
//                            }
//
//                            @Override
//                            public void onBitmapFailed(Drawable errorDrawable) {
//                            }
//
//                            @Override
//                            public void onPrepareLoad(Drawable placeHolderDrawable) {
//                            }
//                        });
//                Picasso.with(getContext())
//                        .load(event.getImages().get(0).getUrl())
//                        .resize(getView().getWidth() - (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,8,getResources().getDisplayMetrics()), (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,200,getResources().getDisplayMetrics()))
//                        .centerCrop()
//                        .into(new Target() {
//                            @Override
//                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream2);
//                            }
//
//                            @Override
//                            public void onBitmapFailed(Drawable errorDrawable) {
//                            }
//
//                            @Override
//                            public void onPrepareLoad(Drawable placeHolderDrawable) {
//                            }
//                        });
                if (event.getEmbedded() != null && event.getEmbedded().getVenues() != null && event.getEmbedded().getVenues().get(0) != null) {
                    EventsModel.Embedded.Event.Embedded_.Venue venue = event.getEmbedded().getVenues().get(0);
                    eventsDatabase = new EventsDatabase(
                            event.getId(),
                            event.getName(),
                            (venue.getLocation() != null && venue.getLocation().getLatitude() != null) ? venue.getLocation().getLatitude() : "",
                            (venue.getLocation() != null && venue.getLocation().getLatitude() != null) ? venue.getLocation().getLongitude() : "",
                            event.getDates().getStart().getLocalDate(),
                            venue.getName(),
                            (venue.getAddress() != null) ? venue.getAddress().getLine1() : "",
                            event.getSales().getPublic().getStartDateTime(),
                            event.getSales().getPublic().getEndDateTime(),
                            (event.getInfo() != null) ? event.getInfo() : "",
                            stream1.toByteArray(),
                            stream2.toByteArray());

                    realmController.saveEventData(eventsDatabase);
                }
            }

            rvList.hideShimmerAdapter();
            rvList.setAdapter(new ListAdapter(eventsModel, R.layout.row, getActivity().getApplicationContext(), (holder, id) -> {

                DetailFragment detailFragment = new DetailFragment();

                detailFragment.setEnterTransition(new Slide(80));
                setExitTransition(new Slide(48));

                Bundle data = new Bundle();
                data.putString("ID", id);
                detailFragment.setArguments(data);

                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, detailFragment)
                        .addToBackStack(null)
                        .commit();
            }));
        } else {
            rvList.setVisibility(View.INVISIBLE);
            cvNoResults.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFetchDataCompleted(ClassificationsModel classificationsModel) {
        classifications.add(getString(R.string.search_none));
        classificationIDs.add("");

        for(ClassificationsModel.Embedded.Classification classification : classificationsModel.getEmbedded().getClassifications()) {
            if (classification.getSegment() != null) {
                classifications.add(classification.getSegment().getName());
                classificationIDs.add(classification.getSegment().getId());
                for (int j = 0; j < classification.getSegment().getEmbedded().getGenres().size(); j++) {
                    classifications.add("- " + classification.getSegment().getEmbedded().getGenres().get(j).getName());
                    classificationIDs.add(classification.getSegment().getEmbedded().getGenres().get(j).getId());
                }
            }
        }

        ArrayAdapter<String> spnrClassificationsAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, classifications);
        spnrClassificationsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnrClassifications.setAdapter(spnrClassificationsAdapter);

        Log.d("ListFragment", "Classification: " + getActivity().getIntent().getStringExtra("CLASSIFICATION"));

        if (((MainActivity) getActivity()).userLocation != null) {
            String latlong = ((MainActivity) getActivity()).userLocation.getLatitude() + "," + ((MainActivity) getActivity()).userLocation.getLongitude();
            Log.i("FINALPROJECT","Searching with Keyword: " + getActivity().getIntent().getStringExtra("KEYWORD"));
            listMVPViewListPresenter.onEventsViewPrepared("" /*getActivity().getIntent().getStringExtra("KEYWORD")*/, latlong, 10, "", 100, 0, "", "", getActivity().getIntent().getStringExtra("CLASSIFICATION"), "");
        } else {
            Log.i("FINALPROJECT","Searching with Keyword: " + getActivity().getIntent().getStringExtra("KEYWORD"));
            listMVPViewListPresenter.onEventsViewPrepared(getActivity().getIntent().getStringExtra("KEYWORD"), "", 10, "", 100, 0, "", "", getActivity().getIntent().getStringExtra("CLASSIFICATION"), "");
        }
    }

    @Override
    public void onError(String message) {
        super.onError(message);
        Log.i("FINALPROJECT", "ListFragment:onError:" + message);
    }

    public IActivityComponent getIFragmentComponent() {
        return mIActivityComponent;
    }

    public interface OnItemClickListener {
        void onItemClick(ListAdapter.ListViewHolder holder, String id);
    }
}
