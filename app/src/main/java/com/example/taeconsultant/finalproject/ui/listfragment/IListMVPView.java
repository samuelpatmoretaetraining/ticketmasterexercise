package com.example.taeconsultant.finalproject.ui.listfragment;

import com.example.taeconsultant.finalproject.data.network.models.ClassificationsModel;
import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.example.taeconsultant.finalproject.ui.base.IMVPView;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public interface IListMVPView extends IMVPView {

    void onFetchDataCompleted(EventsModel eventsModel);

    void onFetchDataCompleted(ClassificationsModel classificationsModel);
}
