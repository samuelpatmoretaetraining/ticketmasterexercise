package com.example.taeconsultant.finalproject.di.module;

import android.app.Activity;
import android.content.Context;

import com.example.taeconsultant.finalproject.data.AppDataManager;
import com.example.taeconsultant.finalproject.data.IDataManager;
import com.example.taeconsultant.finalproject.di.scope.ActivityContext;
import com.example.taeconsultant.finalproject.ui.base.BaseFragment;
import com.example.taeconsultant.finalproject.utility.rx.AppSchedulerProvider;
import com.example.taeconsultant.finalproject.utility.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by samuelpatmore on 29/01/2018.
 */

@Module
public class CloudActivityModule {

    Activity baseActivity;

    public CloudActivityModule(Activity baseActivity) {
        this.baseActivity = baseActivity;
    }

    @Provides
    CompositeDisposable compositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider schedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    IDataManager dataManagerProvider() {
        return new AppDataManager();
    }

}
