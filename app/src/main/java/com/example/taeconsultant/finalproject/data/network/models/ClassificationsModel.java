package com.example.taeconsultant.finalproject.data.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClassificationsModel {

    @SerializedName("_embedded")
    @Expose
    private Embedded embedded;
    @SerializedName("_links")
    @Expose
    private Links____ links;
    @SerializedName("page")
    @Expose
    private Page page;

    public Embedded getEmbedded() {
        return embedded;
    }

    public Links____ getLinks() {
        return links;
    }

    public Page getPage() {
        return page;
    }

    public class Embedded {

        @SerializedName("classifications")
        @Expose
        private List<Classification> classifications = null;

        public List<Classification> getClassifications() {
            return classifications;
        }

        public class Classification {

            @SerializedName("_links")
            @Expose
            private Links links;
            @SerializedName("segment")
            @Expose
            private Segment segment;

            public Links getLinks() {
                return links;
            }

            public Segment getSegment() {
                return segment;
            }

            public class Links {

                @SerializedName("self")
                @Expose
                private Self self;

                public Self getSelf() {
                    return self;
                }

                public class Self {

                    @SerializedName("href")
                    @Expose
                    private String href;

                    public String getHref() {
                        return href;
                    }

                }
            }

            public class Segment {

                @SerializedName("id")
                @Expose
                private String id;
                @SerializedName("name")
                @Expose
                private String name;
                @SerializedName("_links")
                @Expose
                private Links_ links;
                @SerializedName("_embedded")
                @Expose
                private Embedded_ embedded;

                public String getId() {
                    return id;
                }

                public String getName() {
                    return name;
                }

                public Links_ getLinks() {
                    return links;
                }

                public Embedded_ getEmbedded() {
                    return embedded;
                }

                public class Links_ {

                    @SerializedName("self")
                    @Expose
                    private Self_ self;

                    public Self_ getSelf() {
                        return self;
                    }

                    public class Self_ {

                        @SerializedName("href")
                        @Expose
                        private String href;

                        public String getHref() {
                            return href;
                        }

                    }
                }

                public class Embedded_ {

                    @SerializedName("genres")
                    @Expose
                    private List<Genre> genres = null;

                    public List<Genre> getGenres() {
                        return genres;
                    }

                    public class Genre {

                        @SerializedName("id")
                        @Expose
                        private String id;
                        @SerializedName("name")
                        @Expose
                        private String name;
                        @SerializedName("_links")
                        @Expose
                        private Links__ links;
                        @SerializedName("_embedded")
                        @Expose
                        private Embedded__ embedded;

                        public String getId() {
                            return id;
                        }

                        public String getName() {
                            return name;
                        }

                        public Links__ getLinks() {
                            return links;
                        }

                        public Embedded__ getEmbedded() {
                            return embedded;
                        }

                        public class Links__ {

                            @SerializedName("self")
                            @Expose
                            private Self__ self;

                            public Self__ getSelf() {
                                return self;
                            }

                            public class Self__ {

                                @SerializedName("href")
                                @Expose
                                private String href;

                                public String getHref() {
                                    return href;
                                }

                            }
                        }

                        public class Embedded__ {

                            @SerializedName("subgenres")
                            @Expose
                            private List<Subgenre> subgenres = null;

                            public List<Subgenre> getSubgenres() {
                                return subgenres;
                            }

                            public class Subgenre {

                                @SerializedName("id")
                                @Expose
                                private String id;
                                @SerializedName("name")
                                @Expose
                                private String name;
                                @SerializedName("_links")
                                @Expose
                                private Links___ links;

                                public String getId() {
                                    return id;
                                }

                                public String getName() {
                                    return name;
                                }

                                public Links___ getLinks() {
                                    return links;
                                }

                                public class Links___ {

                                    @SerializedName("self")
                                    @Expose
                                    private Self___ self;

                                    public Self___ getSelf() {
                                        return self;
                                    }

                                    public class Self___ {

                                        @SerializedName("href")
                                        @Expose
                                        private String href;

                                        public String getHref() {
                                            return href;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public class Links____ {

        @SerializedName("self")
        @Expose
        private Self____ self;

        public Self____ getSelf() {
            return self;
        }

        public class Self____ {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }
    }

    public class Page {

        @SerializedName("size")
        @Expose
        private Integer size;
        @SerializedName("totalElements")
        @Expose
        private Integer totalElements;
        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("number")
        @Expose
        private Integer number;

        public Integer getSize() {
            return size;
        }

        public Integer getTotalElements() {
            return totalElements;
        }

        public Integer getTotalPages() {
            return totalPages;
        }

        public Integer getNumber() {
            return number;
        }

    }
}