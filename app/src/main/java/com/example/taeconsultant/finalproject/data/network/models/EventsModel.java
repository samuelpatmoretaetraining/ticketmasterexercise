package com.example.taeconsultant.finalproject.data.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EventsModel {

    @SerializedName("_embedded")
    @Expose
    private Embedded embedded;
    @SerializedName("_links")
    @Expose
    private Links links;
    @SerializedName("page")
    @Expose
    private Page page;

    public Embedded getEmbedded() {
        return embedded;
    }

    public Links getLinks() {
        return links;
    }

    public Page getPage() {
        return page;
    }

    public static class Embedded {

        @SerializedName("events")
        @Expose
        private List<Event> events = null;

        public List<Event> getEvents() {
            return events;
        }

        public class Event {

            @SerializedName("genre")
            @Expose
            private String name;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("test")
            @Expose
            private Boolean test;
            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("locale")
            @Expose
            private String locale;
            @SerializedName("images")
            @Expose
            private List<Image> images = null;
            @SerializedName("sales")
            @Expose
            private Sales sales;
            @SerializedName("dates")
            @Expose
            private Dates dates;
            @SerializedName("classifications")
            @Expose
            private List<Classification> classifications = null;
            @SerializedName("promoter")
            @Expose
            private Promoter promoter;
            @SerializedName("promoters")
            @Expose
            private List<Promoter> promoters = null;
            @SerializedName("info")
            @Expose
            private String info;
            @SerializedName("pleaseNote")
            @Expose
            private String pleaseNote;
            @SerializedName("priceRanges")
            @Expose
            private List<PriceRange> priceRanges = null;
            @SerializedName("seatmap")
            @Expose
            private Seatmap seatmap;
            @SerializedName("accessibility")
            @Expose
            private Accessibility accessibility;
            @SerializedName("_links")
            @Expose
            private Links links;
            @SerializedName("_embedded")
            @Expose
            private Embedded_ embedded;

            public String getName() {
                return name;
            }

            public String getType() {
                return type;
            }

            public String getId() {
                return id;
            }

            public Boolean getTest() {
                return test;
            }

            public String getUrl() {
                return url;
            }

            public String getLocale() {
                return locale;
            }

            public List<Image> getImages() {
                return images;
            }

            public Sales getSales() {
                return sales;
            }

            public Dates getDates() {
                return dates;
            }

            public List<Classification> getClassifications() {
                return classifications;
            }

            public Promoter getPromoter() {
                return promoter;
            }

            public List<Promoter> getPromoters() {
                return promoters;
            }

            public String getInfo() {
                return info;
            }

            public String getPleaseNote() {
                return pleaseNote;
            }

            public List<PriceRange> getPriceRanges() {
                return priceRanges;
            }

            public Seatmap getSeatmap() {
                return seatmap;
            }

            public Accessibility getAccessibility() {
                return accessibility;
            }

            public Links getLinks() {
                return links;
            }

            public Embedded_ getEmbedded() {
                return embedded;
            }

            public class Embedded_ {

                @SerializedName("venues")
                @Expose
                private List<Venue> venues = null;
                @SerializedName("attractions")
                @Expose
                private List<Attraction> attractions = null;

                public List<Venue> getVenues() {
                    return venues;
                }

                public List<Attraction> getAttractions() {
                    return attractions;
                }

                public class Attraction {

                    @SerializedName("genre")
                    @Expose
                    private String name;
                    @SerializedName("type")
                    @Expose
                    private String type;
                    @SerializedName("id")
                    @Expose
                    private String id;
                    @SerializedName("test")
                    @Expose
                    private Boolean test;
                    @SerializedName("url")
                    @Expose
                    private String url;
                    @SerializedName("locale")
                    @Expose
                    private String locale;
                    @SerializedName("images")
                    @Expose
                    private List<Image> images = null;
                    @SerializedName("classifications")
                    @Expose
                    private List<Classification> classifications = null;
                    @SerializedName("upcomingEvents")
                    @Expose
                    private UpcomingEvents upcomingEvents;
                    @SerializedName("_links")
                    @Expose
                    private Links links;

                    public String getName() {
                        return name;
                    }

                    public String getType() {
                        return type;
                    }

                    public String getId() {
                        return id;
                    }

                    public Boolean getTest() {
                        return test;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public String getLocale() {
                        return locale;
                    }

                    public List<Image> getImages() {
                        return images;
                    }

                    public List<Classification> getClassifications() {
                        return classifications;
                    }

                    public UpcomingEvents getUpcomingEvents() {
                        return upcomingEvents;
                    }

                    public Links getLinks() {
                        return links;
                    }

                    public class UpcomingEvents {

                        @SerializedName("_total")
                        @Expose
                        private Integer total;
                        @SerializedName("tmr")
                        @Expose
                        private Integer tmr;
                        @SerializedName("ticketmaster")
                        @Expose
                        private Integer ticketmaster;

                        public Integer getTotal() {
                            return total;
                        }

                        public Integer getTmr() {
                            return tmr;
                        }

                        public Integer getTicketmaster() {
                            return ticketmaster;
                        }

                    }

                    public class Classification {

                        @SerializedName("primary")
                        @Expose
                        private Boolean primary;
                        @SerializedName("segment")
                        @Expose
                        private Segment segment;
                        @SerializedName("genre")
                        @Expose
                        private Genre genre;
                        @SerializedName("subGenre")
                        @Expose
                        private SubGenre subGenre;
                        @SerializedName("type")
                        @Expose
                        private Type type;
                        @SerializedName("subType")
                        @Expose
                        private SubType subType;

                        public Boolean getPrimary() {
                            return primary;
                        }

                        public Segment getSegment() {
                            return segment;
                        }

                        public Genre getGenre() {
                            return genre;
                        }

                        public SubGenre getSubGenre() {
                            return subGenre;
                        }

                        public Type getType() {
                            return type;
                        }

                        public SubType getSubType() {
                            return subType;
                        }

                        public class Type {

                            @SerializedName("id")
                            @Expose
                            private String id;
                            @SerializedName("genre")
                            @Expose
                            private String name;

                            public String getId() {
                                return id;
                            }

                            public String getName() {
                                return name;
                            }

                        }

                        public class SubType {

                            @SerializedName("id")
                            @Expose
                            private String id;
                            @SerializedName("genre")
                            @Expose
                            private String name;

                            public String getId() {
                                return id;
                            }

                            public String getName() {
                                return name;
                            }

                        }

                        public class SubGenre {

                            @SerializedName("id")
                            @Expose
                            private String id;
                            @SerializedName("genre")
                            @Expose
                            private String name;

                            public String getId() {
                                return id;
                            }

                            public String getName() {
                                return name;
                            }

                        }

                        public class Segment {

                            @SerializedName("id")
                            @Expose
                            private String id;
                            @SerializedName("genre")
                            @Expose
                            private String name;

                            public String getId() {
                                return id;
                            }

                            public String getName() {
                                return name;
                            }

                        }

                        public class Genre {

                            @SerializedName("id")
                            @Expose
                            private String id;
                            @SerializedName("genre")
                            @Expose
                            private String name;

                            public String getId() {
                                return id;
                            }

                            public String getName() {
                                return name;
                            }

                        }
                    }

                    public class Image {

                        @SerializedName("ratio")
                        @Expose
                        private String ratio;
                        @SerializedName("url")
                        @Expose
                        private String url;
                        @SerializedName("width")
                        @Expose
                        private Integer width;
                        @SerializedName("height")
                        @Expose
                        private Integer height;
                        @SerializedName("fallback")
                        @Expose
                        private Boolean fallback;

                        public String getRatio() {
                            return ratio;
                        }

                        public String getUrl() {
                            return url;
                        }

                        public Integer getWidth() {
                            return width;
                        }

                        public Integer getHeight() {
                            return height;
                        }

                        public Boolean getFallback() {
                            return fallback;
                        }

                    }

                    public class Links {

                        @SerializedName("self")
                        @Expose
                        private Self self;

                        public Self getSelf() {
                            return self;
                        }

                        public class Self {

                            @SerializedName("href")
                            @Expose
                            private String href;

                            public String getHref() {
                                return href;
                            }

                        }
                    }
                }

                public class Venue {

                    @SerializedName("genre")
                    @Expose
                    private String name;
                    @SerializedName("type")
                    @Expose
                    private String type;
                    @SerializedName("id")
                    @Expose
                    private String id;
                    @SerializedName("test")
                    @Expose
                    private Boolean test;
                    @SerializedName("url")
                    @Expose
                    private String url;
                    @SerializedName("locale")
                    @Expose
                    private String locale;
                    @SerializedName("images")
                    @Expose
                    private List<Image> images = null;
                    @SerializedName("postalCode")
                    @Expose
                    private String postalCode;
                    @SerializedName("timezone")
                    @Expose
                    private String timezone;
                    @SerializedName("city")
                    @Expose
                    private City city;
                    @SerializedName("state")
                    @Expose
                    private State state;
                    @SerializedName("country")
                    @Expose
                    private Country country;
                    @SerializedName("address")
                    @Expose
                    private Address address;
                    @SerializedName("location")
                    @Expose
                    private Location location;
                    @SerializedName("markets")
                    @Expose
                    private List<Market> markets = null;
                    @SerializedName("dmas")
                    @Expose
                    private List<Dma> dmas = null;
                    @SerializedName("social")
                    @Expose
                    private Social social;
                    @SerializedName("boxOfficeInfo")
                    @Expose
                    private BoxOfficeInfo boxOfficeInfo;
                    @SerializedName("parkingDetail")
                    @Expose
                    private String parkingDetail;
                    @SerializedName("accessibleSeatingDetail")
                    @Expose
                    private String accessibleSeatingDetail;
                    @SerializedName("generalInfo")
                    @Expose
                    private GeneralInfo generalInfo;
                    @SerializedName("upcomingEvents")
                    @Expose
                    private UpcomingEvents upcomingEvents;
                    @SerializedName("_links")
                    @Expose
                    private Links links;

                    public String getName() {
                        return name;
                    }

                    public String getType() {
                        return type;
                    }

                    public String getId() {
                        return id;
                    }

                    public Boolean getTest() {
                        return test;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public String getLocale() {
                        return locale;
                    }

                    public List<Image> getImages() {
                        return images;
                    }

                    public String getPostalCode() {
                        return postalCode;
                    }

                    public String getTimezone() {
                        return timezone;
                    }

                    public City getCity() {
                        return city;
                    }

                    public State getState() {
                        return state;
                    }

                    public Country getCountry() {
                        return country;
                    }

                    public Address getAddress() {
                        return address;
                    }

                    public Location getLocation() {
                        return location;
                    }

                    public List<Market> getMarkets() {
                        return markets;
                    }

                    public List<Dma> getDmas() {
                        return dmas;
                    }

                    public Social getSocial() {
                        return social;
                    }

                    public BoxOfficeInfo getBoxOfficeInfo() {
                        return boxOfficeInfo;
                    }

                    public String getParkingDetail() {
                        return parkingDetail;
                    }

                    public String getAccessibleSeatingDetail() {
                        return accessibleSeatingDetail;
                    }

                    public GeneralInfo getGeneralInfo() {
                        return generalInfo;
                    }

                    public UpcomingEvents getUpcomingEvents() {
                        return upcomingEvents;
                    }

                    public Links getLinks() {
                        return links;
                    }

                    public class UpcomingEvents {

                        @SerializedName("_total")
                        @Expose
                        private Integer total;
                        @SerializedName("tmr")
                        @Expose
                        private Integer tmr;
                        @SerializedName("ticketmaster")
                        @Expose
                        private Integer ticketmaster;

                        public Integer getTotal() {
                            return total;
                        }

                        public Integer getTmr() {
                            return tmr;
                        }

                        public Integer getTicketmaster() {
                            return ticketmaster;
                        }

                    }

                    public class Social {

                        @SerializedName("twitter")
                        @Expose
                        private Twitter twitter;

                        public Twitter getTwitter() {
                            return twitter;
                        }

                        public class Twitter {

                            @SerializedName("handle")
                            @Expose
                            private String handle;

                            public String getHandle() {
                                return handle;
                            }

                        }
                    }

                    public class State {

                        @SerializedName("genre")
                        @Expose
                        private String name;
                        @SerializedName("stateCode")
                        @Expose
                        private String stateCode;

                        public String getName() {
                            return name;
                        }

                        public String getStateCode() {
                            return stateCode;
                        }

                    }

                    public class Address {

                        @SerializedName("line1")
                        @Expose
                        private String line1;

                        public String getLine1() {
                            return line1;
                        }

                    }

                    public class BoxOfficeInfo {

                        @SerializedName("phoneNumberDetail")
                        @Expose
                        private String phoneNumberDetail;
                        @SerializedName("openHoursDetail")
                        @Expose
                        private String openHoursDetail;
                        @SerializedName("acceptedPaymentDetail")
                        @Expose
                        private String acceptedPaymentDetail;
                        @SerializedName("willCallDetail")
                        @Expose
                        private String willCallDetail;

                        public String getPhoneNumberDetail() {
                            return phoneNumberDetail;
                        }

                        public String getOpenHoursDetail() {
                            return openHoursDetail;
                        }

                        public String getAcceptedPaymentDetail() {
                            return acceptedPaymentDetail;
                        }

                        public String getWillCallDetail() {
                            return willCallDetail;
                        }

                    }

                    public class City {

                        @SerializedName("genre")
                        @Expose
                        private String name;

                        public String getName() {
                            return name;
                        }

                    }

                    public class Country {

                        @SerializedName("genre")
                        @Expose
                        private String name;
                        @SerializedName("countryCode")
                        @Expose
                        private String countryCode;

                        public String getName() {
                            return name;
                        }

                        public String getCountryCode() {
                            return countryCode;
                        }

                    }

                    public class Dma {

                        @SerializedName("id")
                        @Expose
                        private Integer id;

                        public Integer getId() {
                            return id;
                        }

                    }

                    public class GeneralInfo {

                        @SerializedName("generalRule")
                        @Expose
                        private String generalRule;
                        @SerializedName("childRule")
                        @Expose
                        private String childRule;

                        public String getGeneralRule() {
                            return generalRule;
                        }

                        public String getChildRule() {
                            return childRule;
                        }

                    }

                    public class Image {

                        @SerializedName("ratio")
                        @Expose
                        private String ratio;
                        @SerializedName("url")
                        @Expose
                        private String url;
                        @SerializedName("width")
                        @Expose
                        private Integer width;
                        @SerializedName("height")
                        @Expose
                        private Integer height;
                        @SerializedName("fallback")
                        @Expose
                        private Boolean fallback;
                        @SerializedName("attribution")
                        @Expose
                        private String attribution;

                        public String getRatio() {
                            return ratio;
                        }

                        public String getUrl() {
                            return url;
                        }

                        public Integer getWidth() {
                            return width;
                        }

                        public Integer getHeight() {
                            return height;
                        }

                        public Boolean getFallback() {
                            return fallback;
                        }

                        public String getAttribution() {
                            return attribution;
                        }

                    }

                    public class Links {

                        @SerializedName("self")
                        @Expose
                        private Self self;

                        public Self getSelf() {
                            return self;
                        }

                        public class Self {

                            @SerializedName("href")
                            @Expose
                            private String href;

                            public String getHref() {
                                return href;
                            }

                        }
                    }

                    public class Location {

                        @SerializedName("longitude")
                        @Expose
                        private String longitude;
                        @SerializedName("latitude")
                        @Expose
                        private String latitude;

                        public String getLongitude() {
                            return longitude;
                        }

                        public String getLatitude() {
                            return latitude;
                        }

                    }

                    public class Market {

                        @SerializedName("id")
                        @Expose
                        private String id;

                        public String getId() {
                            return id;
                        }

                    }
                }
            }

            public class Classification {

                @SerializedName("primary")
                @Expose
                private Boolean primary;
                @SerializedName("segment")
                @Expose
                private Segment segment;
                @SerializedName("genre")
                @Expose
                private Genre genre;
                @SerializedName("subGenre")
                @Expose
                private SubGenre subGenre;
                @SerializedName("type")
                @Expose
                private Type type;
                @SerializedName("subType")
                @Expose
                private SubType subType;

                public Boolean getPrimary() {
                    return primary;
                }

                public Segment getSegment() {
                    return segment;
                }

                public Genre getGenre() {
                    return genre;
                }

                public SubGenre getSubGenre() {
                    return subGenre;
                }

                public Type getType() {
                    return type;
                }

                public SubType getSubType() {
                    return subType;
                }

                public class SubType {

                    @SerializedName("id")
                    @Expose
                    private String id;
                    @SerializedName("genre")
                    @Expose
                    private String name;

                    public String getId() {
                        return id;
                    }

                    public String getName() {
                        return name;
                    }

                }

                public class SubGenre {

                    @SerializedName("id")
                    @Expose
                    private String id;
                    @SerializedName("genre")
                    @Expose
                    private String name;

                    public String getId() {
                        return id;
                    }

                    public String getName() {
                        return name;
                    }

                }

                public class Segment {

                    @SerializedName("id")
                    @Expose
                    private String id;
                    @SerializedName("genre")
                    @Expose
                    private String name;

                    public String getId() {
                        return id;
                    }

                    public String getName() {
                        return name;
                    }

                }

                public class Genre {

                    @SerializedName("id")
                    @Expose
                    private String id;
                    @SerializedName("genre")
                    @Expose
                    private String name;

                    public String getId() {
                        return id;
                    }

                    public String getName() {
                        return name;
                    }

                }

                public class Type {

                    @SerializedName("id")
                    @Expose
                    private String id;
                    @SerializedName("genre")
                    @Expose
                    private String name;

                    public String getId() {
                        return id;
                    }

                    public String getName() {
                        return name;
                    }

                }
            }

            public class Dates {

                @SerializedName("start")
                @Expose
                private Start start;
                @SerializedName("timezone")
                @Expose
                private String timezone;
                @SerializedName("status")
                @Expose
                private Status status;
                @SerializedName("spanMultipleDays")
                @Expose
                private Boolean spanMultipleDays;

                public Start getStart() {
                    return start;
                }

                public String getTimezone() {
                    return timezone;
                }

                public Status getStatus() {
                    return status;
                }

                public Boolean getSpanMultipleDays() {
                    return spanMultipleDays;
                }

                public class Status {

                    @SerializedName("code")
                    @Expose
                    private String code;

                    public String getCode() {
                        return code;
                    }

                }

                public class Start {

                    @SerializedName("localDate")
                    @Expose
                    private String localDate;
                    @SerializedName("localTime")
                    @Expose
                    private String localTime;
                    @SerializedName("dateTime")
                    @Expose
                    private String dateTime;
                    @SerializedName("dateTBD")
                    @Expose
                    private Boolean dateTBD;
                    @SerializedName("dateTBA")
                    @Expose
                    private Boolean dateTBA;
                    @SerializedName("timeTBA")
                    @Expose
                    private Boolean timeTBA;
                    @SerializedName("noSpecificTime")
                    @Expose
                    private Boolean noSpecificTime;

                    public String getLocalDate() {
                        return localDate;
                    }

                    public String getLocalTime() {
                        return localTime;
                    }

                    public String getDateTime() {
                        return dateTime;
                    }

                    public Boolean getDateTBD() {
                        return dateTBD;
                    }

                    public Boolean getDateTBA() {
                        return dateTBA;
                    }

                    public Boolean getTimeTBA() {
                        return timeTBA;
                    }

                    public Boolean getNoSpecificTime() {
                        return noSpecificTime;
                    }

                }
            }

            public class Links {

                @SerializedName("self")
                @Expose
                private Self self;
                @SerializedName("attractions")
                @Expose
                private List<Attraction> attractions = null;
                @SerializedName("venues")
                @Expose
                private List<Venue> venues = null;

                public Self getSelf() {
                    return self;
                }

                public List<Attraction> getAttractions() {
                    return attractions;
                }

                public List<Venue> getVenues() {
                    return venues;
                }

                public class Self {

                    @SerializedName("href")
                    @Expose
                    private String href;

                    public String getHref() {
                        return href;
                    }

                }

                public class Attraction {

                    @SerializedName("href")
                    @Expose
                    private String href;

                    public String getHref() {
                        return href;
                    }

                }

                public class Venue {

                    @SerializedName("href")
                    @Expose
                    private String href;

                    public String getHref() {
                        return href;
                    }

                }
            }

            public class Seatmap {

                @SerializedName("staticUrl")
                @Expose
                private String staticUrl;

                public String getStaticUrl() {
                    return staticUrl;
                }

            }

            public class Sales {

                @SerializedName("public")
                @Expose
                private Public _public;

                public Public getPublic() {
                    return _public;
                }

                public class Public {

                    @SerializedName("startDateTime")
                    @Expose
                    private String startDateTime;
                    @SerializedName("startTBD")
                    @Expose
                    private Boolean startTBD;
                    @SerializedName("endDateTime")
                    @Expose
                    private String endDateTime;

                    public String getStartDateTime() {
                        return startDateTime;
                    }

                    public Boolean getStartTBD() {
                        return startTBD;
                    }

                    public String getEndDateTime() {
                        return endDateTime;
                    }

                }
            }

            public class Promoter {

                @SerializedName("id")
                @Expose
                private String id;
                @SerializedName("genre")
                @Expose
                private String name;
                @SerializedName("description")
                @Expose
                private String description;

                public String getId() {
                    return id;
                }

                public String getName() {
                    return name;
                }

                public String getDescription() {
                    return description;
                }

            }

            public class PriceRange {

                @SerializedName("type")
                @Expose
                private String type;
                @SerializedName("currency")
                @Expose
                private String currency;
                @SerializedName("min")
                @Expose
                private Double min;
                @SerializedName("max")
                @Expose
                private Double max;

                public String getType() {
                    return type;
                }

                public String getCurrency() {
                    return currency;
                }

                public Double getMin() {
                    return min;
                }

                public Double getMax() {
                    return max;
                }

            }

            public class Accessibility {

                @SerializedName("info")
                @Expose
                private String info;

                public String getInfo() {
                    return info;
                }

            }

            public class Image {

                @SerializedName("ratio")
                @Expose
                private String ratio;
                @SerializedName("url")
                @Expose
                private String url;
                @SerializedName("width")
                @Expose
                private Integer width;
                @SerializedName("height")
                @Expose
                private Integer height;
                @SerializedName("fallback")
                @Expose
                private Boolean fallback;

                public String getRatio() {
                    return ratio;
                }

                public String getUrl() {
                    return url;
                }

                public Integer getWidth() {
                    return width;
                }

                public Integer getHeight() {
                    return height;
                }

                public Boolean getFallback() {
                    return fallback;
                }

            }
        }

    }

    public static class Links {

        @SerializedName("first")
        @Expose
        private First first;
        @SerializedName("self")
        @Expose
        private Self self;
        @SerializedName("next")
        @Expose
        private Next next;
        @SerializedName("last")
        @Expose
        private Last last;

        public First getFirst() {
            return first;
        }

        public Self getSelf() {
            return self;
        }

        public Next getNext() {
            return next;
        }

        public Last getLast() {
            return last;
        }

        public class Self {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }

        public class Next {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }

        public class First {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }

        public class Last {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }
    }

    public static class Page {

        @SerializedName("size")
        @Expose
        private Integer size;
        @SerializedName("totalElements")
        @Expose
        private Integer totalElements;
        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("number")
        @Expose
        private Integer number;

        public Integer getSize() {
            return size;
        }

        public Integer getTotalElements() {
            return totalElements;
        }

        public Integer getTotalPages() {
            return totalPages;
        }

        public Integer getNumber() {
            return number;
        }

    }
}