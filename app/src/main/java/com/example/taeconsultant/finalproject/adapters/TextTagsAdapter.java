package com.example.taeconsultant.finalproject.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taeconsultant.finalproject.Classification;
import com.example.taeconsultant.finalproject.MainActivity;
import com.moxun.tagcloudlib.view.TagsAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by moxun on 16/1/19.
 */
public class TextTagsAdapter extends TagsAdapter {

    private static final String MUSIC = "Music", FILM = "Film", SPORTS = "Sports", ARTS = "Arts & Theatre";
    private static final int
            MUSIC_COLOR = Color.parseColor("#ffcb5b"),
            FILM_COLOR = Color.parseColor("#4286f4"),
            SPORTS_COLOR = Color.parseColor("#FF0000"),
            ARTS_COLOR = Color.parseColor("#d34fff");

    private List<Classification> dataSet = new ArrayList<>();

    public TextTagsAdapter(@NotNull List<Classification> classifications) {
        dataSet.clear();
        dataSet.addAll(classifications);
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public View getView(final Context context, final int position, ViewGroup parent) {
        TextView tv = new TextView(context);
        tv.setText(dataSet.get(position).genre);
        tv.setBackgroundColor(Color.TRANSPARENT);
        tv.setGravity(Gravity.CENTER);

        if (dataSet.get(position).segment.equals(dataSet.get(position).genre)) {
            tv.setTextSize(20f);
        }

        tv.setTextColor(Color.WHITE);
        switch (dataSet.get(position).segment) {
            case MUSIC :
                tv.setTextColor(MUSIC_COLOR);
                break;
            case FILM :
                tv.setTextColor(FILM_COLOR);
                break;
            case SPORTS :
                tv.setTextColor(SPORTS_COLOR);
                break;
            case ARTS :
                tv.setTextColor(ARTS_COLOR);
                break;
        }

        tv.setOnClickListener(v -> {
            Log.e("Click", dataSet.get(position) + " clicked.");
//            Toast.makeText(context, dataSet.get(position) + " clicked", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(context, MainActivity.class);

            String id = dataSet.get(position).id;
            intent.putExtra("CLASSIFICATION",id);
            context.startActivity(intent);
        });
        return tv;
    }

    @Override
    public Object getItem(int position) {
        return dataSet.get(position);
    }

    @Override
    public int getPopularity(int position) {
        return position % 7;
    }

    @Override
    public void onThemeColorChanged(View view, int themeColor) {
//        view.setBackgroundColor(themeColor);
        view.setBackgroundColor(Color.TRANSPARENT);
    }
}