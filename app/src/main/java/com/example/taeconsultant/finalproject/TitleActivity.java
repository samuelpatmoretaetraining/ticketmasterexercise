package com.example.taeconsultant.finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.yalantis.filter.adapter.FilterAdapter;
import com.yalantis.filter.listener.FilterListener;
import com.yalantis.filter.widget.Filter;
import com.yalantis.filter.widget.FilterItem;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TitleActivity extends AppCompatActivity implements FilterListener<Tag>{

    Unbinder unbinder;

    ArrayList<Tag> selectedTags = new ArrayList<>();

    List<String> filterTitles = new ArrayList<>();
    List<Integer> filterColours = new ArrayList<>();

    @BindView(R.id.filter)
    Filter fltrKeywords;
    @BindView(R.id.btnTitleSearch)
    Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title);

        unbinder = ButterKnife.bind(this);

        filterTitles.add("comic");
        filterColours.add(0xFFFF0000);
        filterTitles.add("music");
        filterColours.add(0xFF00FF00);
        filterTitles.add("film");
        filterColours.add(0xFF0000FF);

        initializeViews();
    }

    private void initializeViews() {
        fltrKeywords.setAdapter(new TitleFilterAdapter(getTags()));
        fltrKeywords.setCollapsedBackground(0x11FFFFFF);
        fltrKeywords.setExpandedBackground(0x11FFFFFF);
        fltrKeywords.setNoSelectedItemText("Popular Keywords");
        fltrKeywords.setListener(this);
        fltrKeywords.expand();
        fltrKeywords.build();

        btnSearch.setOnClickListener(view -> {
            Intent intent = new Intent(TitleActivity.this,MainActivity.class);

            String keyword = "";
            for(int i=0;i<selectedTags.size();i++) {
                keyword = keyword + selectedTags.get(i).getText();
                if(i<selectedTags.size()-1){
                    keyword = keyword + ",";
                }
            }

            intent.putExtra("KEYWORD",keyword);
            startActivity(intent);
        });
    }

    private List<? extends Tag> getTags() {
        List<Tag> tags = new ArrayList<>();

        for(int i=0;i<filterTitles.size();i++) {
            tags.add(new Tag(filterTitles.get(i),filterColours.get(i)));
        }

        return tags;
    }

    @Override
    public void onFiltersSelected(@NotNull ArrayList<Tag> arrayList) {}

    @Override
    public void onNothingSelected() {}

    @Override
    public void onFilterSelected(Tag tag) {
        selectedTags.add(tag);
    }

    @Override
    public void onFilterDeselected(Tag tag) {
        selectedTags.remove(tag);
    }

    class TitleFilterAdapter extends FilterAdapter<Tag> {

        public TitleFilterAdapter(@NotNull List<? extends Tag> items) {
            super(items);
        }

        @NotNull
        @Override
        public FilterItem createView(int position, Tag item) {
            FilterItem filterItem = new FilterItem(TitleActivity.this);

            filterItem.setStrokeColor(0xFFFFFFFF);
            filterItem.setTextColor(0xFFFFFFFF);
            filterItem.setCheckedTextColor(0xFF000000);
            filterItem.setColor(0xFF000000);
            filterItem.setCheckedColor(item.getColour());
            filterItem.setText(item.getText());
            filterItem.deselect();

            return filterItem;
        }
    }
}
