package com.example.taeconsultant.finalproject.ui.listfragment;

import com.example.taeconsultant.finalproject.ui.base.IMVPPresenter;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public interface IListMVPPresenter<V extends IListMVPView> extends IMVPPresenter<V> {

    void onEventsViewPrepared(
            String keyword,
            String latlong,
            int radius,
            String unit,
            int size,
            int page,
            String sort,
            String countryCode,
            String classificationId,
            String includeLicensedContent);

    void onClassificationsViewPrepared();
}
