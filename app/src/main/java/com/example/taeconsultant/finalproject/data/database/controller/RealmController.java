package com.example.taeconsultant.finalproject.data.database.controller;

import com.example.taeconsultant.finalproject.data.database.localdb.EventsDatabase;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by TAE Consultant on 26/10/2017.
 */

public class RealmController {

    Realm realm;

    public RealmController(Realm realm) {
        this.realm = realm;
    }

    public void saveEventData(final EventsDatabase eventsDatabase) {
        realm.executeTransaction(realm1 -> realm1.copyToRealm(eventsDatabase));
    }

    public ArrayList<EventsDatabase> getEventsDatabase() {
        ArrayList<EventsDatabase> eventsDatabaseArrayList = new ArrayList<>();

        RealmResults<EventsDatabase> eventsDatabaseRealmResults = realm.where(EventsDatabase.class).distinct("id");

        for(EventsDatabase eventsDatabase: eventsDatabaseRealmResults) {
            eventsDatabaseArrayList.add(eventsDatabase);
        }

        return eventsDatabaseArrayList;
    }

    public EventsDatabase getEventData(String id) {

        EventsDatabase eventsDatabase = realm.where(EventsDatabase.class).equalTo("id", id).findFirst();

        return eventsDatabase;
    }

    public void deleteDatabase() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });
    }
}
