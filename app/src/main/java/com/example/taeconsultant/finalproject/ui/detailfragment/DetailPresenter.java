package com.example.taeconsultant.finalproject.ui.detailfragment;

import com.example.taeconsultant.finalproject.data.IDataManager;
import com.example.taeconsultant.finalproject.ui.base.BasePresenter;
import com.example.taeconsultant.finalproject.utility.Constants;
import com.example.taeconsultant.finalproject.utility.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public class DetailPresenter<V extends IDetailMVPView> extends BasePresenter<V> implements IDetailMVPPresenter<V> {

    @Inject
    public DetailPresenter(IDataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onEventDetailViewPrepared(String id) {

        getCompositeDisposable()
                .add(getDataManager().useCaseEventDetails(id, Constants.API_KEY)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(event -> getMvpView().onFetchDataCompleted(event),
                                throwable -> getMvpView().onError(throwable.getMessage())
                        )
                );
    }

    @Override
    public void onVenueDetailViewPrepared(String id) {

        getCompositeDisposable()
                .add(getDataManager().useCaseVenueDetails(id, Constants.API_KEY)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(venue -> getMvpView().onFetchDataCompleted(venue),
                                throwable -> getMvpView().onError(throwable.getMessage())
                        )
                );
    }
}
