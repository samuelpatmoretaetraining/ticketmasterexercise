package com.example.taeconsultant.finalproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.taeconsultant.finalproject.MyApplication;
import com.example.taeconsultant.finalproject.R;
import com.example.taeconsultant.finalproject.utility.CircleTransform;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by TAE Consultant on 25/10/2017.
 */

public class VenueInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final Context applicationContext;

    private final View venueContentsView;

    private final ArrayList<String> eventStrings;

    public VenueInfoWindowAdapter(ArrayList<String> eventStrings, Context applicationContext) {
        venueContentsView = LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.venue_info_contents,null);
        this.applicationContext = applicationContext;
        this.eventStrings = eventStrings;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        ImageView ivInfoIcon = venueContentsView.findViewById(R.id.ivInfoIcon);

        TextView tvInfoTitle = venueContentsView.findViewById(R.id.tvInfoTitle);
        TextView tvInfoVenue = venueContentsView.findViewById(R.id.tvInfoVenue);
        TextView tvInfoDate = venueContentsView.findViewById(R.id.tvInfoDate);

        tvInfoTitle.setText(eventStrings.get(0));
        tvInfoVenue.setText(eventStrings.get(2));

        String[] startDate = eventStrings.get(1).split("-");

        tvInfoDate.setText(startDate[2] + "/" + startDate[1] + "/" + startDate[0]);

        Picasso.with(applicationContext)
                .load(eventStrings.get(5))
                .resize(100,100)
                .centerCrop()
                .transform(new CircleTransform())
                .into(ivInfoIcon);

        return venueContentsView;
    }
}
