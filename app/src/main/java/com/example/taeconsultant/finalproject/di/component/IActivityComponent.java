package com.example.taeconsultant.finalproject.di.component;

import com.example.taeconsultant.finalproject.DetailFragment;
import com.example.taeconsultant.finalproject.ListFragment;
import com.example.taeconsultant.finalproject.TagCloudActivity;
import com.example.taeconsultant.finalproject.di.module.ActivityModule;
import com.example.taeconsultant.finalproject.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by TAE Consultant on 03/10/2017.
 */

@PerActivity
@Component(dependencies = IApplicationComponent.class, modules = ActivityModule.class)
public interface IActivityComponent {

    void inject(DetailFragment fragment);
    void inject(ListFragment fragment);
}
