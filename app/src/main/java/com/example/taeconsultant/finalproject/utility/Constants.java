package com.example.taeconsultant.finalproject.utility;

/**
 * Created by TAE Consultant on 17/10/2017.
 */

public class Constants {
    public static final String BASE_URL = "https://app.ticketmaster.com/discovery/v2/";

    public static final String EVENTS_URL = "events";
    public static final String EVENT_DETAIL_URL = "events/{id}";
    public static final String CLASSIFICATIONS_URL = "classifications";
    public static final String VENUE_DETAIL_URL = "venues/{id}";

    public static final String API_KEY = "7elxdku9GGG5k8j0Xm8KWdANDgecHMV0";
}
