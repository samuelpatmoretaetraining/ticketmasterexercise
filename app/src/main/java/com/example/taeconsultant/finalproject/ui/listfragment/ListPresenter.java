package com.example.taeconsultant.finalproject.ui.listfragment;

import android.util.Log;

import com.example.taeconsultant.finalproject.data.IDataManager;
import com.example.taeconsultant.finalproject.ui.base.BasePresenter;
import com.example.taeconsultant.finalproject.utility.Constants;
import com.example.taeconsultant.finalproject.utility.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public class ListPresenter<V extends IListMVPView> extends BasePresenter<V> implements IListMVPPresenter<V> {

    @Inject
    public ListPresenter(IDataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onEventsViewPrepared(
            String keyword,
            String latlong,
            int radius,
            String unit,
            int size,
            int page,
            String sort,
            String countryCode,
            String classificationId,
            String includeLicensedContent) {

        getCompositeDisposable()
                .add(getDataManager().useCaseEvents(Constants.API_KEY, keyword, latlong, radius, unit, size, page, sort, countryCode, classificationId, includeLicensedContent)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(eventsModel -> getMvpView().onFetchDataCompleted(eventsModel),
                                throwable -> {
                                    throwable.printStackTrace();
                                    getMvpView().onError(throwable.getMessage());
                                }
                        )
                );
    }

    @Override
    public void onClassificationsViewPrepared() {
        Log.i("FINALPROJECT","ListPresenter:onClassificationsViewPrepared");

        getCompositeDisposable()
                .add(getDataManager().useCaseClassifications(Constants.API_KEY)
                        .subscribeOn(getSchedulerProvider().io())
                        .observeOn(getSchedulerProvider().ui())
                        .subscribe(classificationsModel -> getMvpView().onFetchDataCompleted(classificationsModel),
                                throwable -> {
                            throwable.printStackTrace();
                            getMvpView().onError(throwable.getMessage());
                                }
                        )
                );
    }
}
