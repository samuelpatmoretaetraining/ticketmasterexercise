package com.example.taeconsultant.finalproject;

import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.taeconsultant.finalproject.adapters.VenueInfoWindowAdapter;
import com.example.taeconsultant.finalproject.ui.base.BaseFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends BaseFragment {

    @BindView(R.id.mapView)
    MapView mapView;

    GoogleMap gMap;

    LatLng userLatLng;

    ArrayList<String> eventStrings = new ArrayList<>();

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        userLatLng = new LatLng(getArguments().getDouble("UserLat"),getArguments().getDouble("UserLng"));

        eventStrings = getArguments().getStringArrayList("EventStrings");

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(googleMap -> {
            Log.i("FINALPROJECT","Map is ready");
            gMap = googleMap;
            gMap.getUiSettings().setMapToolbarEnabled(true);
            gMap.getUiSettings().setCompassEnabled(true);

            gMap.setInfoWindowAdapter(new VenueInfoWindowAdapter(eventStrings, getContext()));

            gMap.addMarker(new MarkerOptions()
                    .position(new LatLng(
                            Double.parseDouble(eventStrings.get(3)),
                            Double.parseDouble(eventStrings.get(4))))
                    .title(eventStrings.get(0))
                    .snippet(eventStrings.get(2)))
                    .showInfoWindow();
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(eventStrings.get(3)), Double.parseDouble(eventStrings.get(4))),12));
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onLowMemory() {
        mapView.onLowMemory();
        super.onLowMemory();
    }
}
