package com.example.taeconsultant.finalproject;

import android.app.Application;
import android.content.Context;

import com.example.taeconsultant.finalproject.di.component.DaggerIApplicationComponent;
import com.example.taeconsultant.finalproject.di.component.IApplicationComponent;
import com.example.taeconsultant.finalproject.di.module.ApplicationModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by TAE Consultant on 17/10/2017.
 */

public class MyApplication extends Application {

    public static Application sApplication;

    IApplicationComponent iApplicationComponent;

    public IApplicationComponent getiApplicationComponent() {
        return iApplicationComponent;
    }

    public static Application getApplication() {
        return sApplication;
    }

    public static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
        iApplicationComponent = DaggerIApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        Realm.init(getApplicationContext());

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);

        getiApplicationComponent().inject(this);
    }
}
