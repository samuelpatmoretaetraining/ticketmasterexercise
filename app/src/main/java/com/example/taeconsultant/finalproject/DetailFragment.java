package com.example.taeconsultant.finalproject;


import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.taeconsultant.finalproject.data.database.controller.RealmController;
import com.example.taeconsultant.finalproject.data.database.localdb.EventsDatabase;
import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.example.taeconsultant.finalproject.data.network.models.VenuesModel;
import com.example.taeconsultant.finalproject.di.component.DaggerIActivityComponent;
import com.example.taeconsultant.finalproject.di.component.IActivityComponent;
import com.example.taeconsultant.finalproject.di.module.ActivityModule;
import com.example.taeconsultant.finalproject.ui.base.BaseFragment;
import com.example.taeconsultant.finalproject.ui.detailfragment.DetailPresenter;
import com.example.taeconsultant.finalproject.ui.detailfragment.IDetailMVPView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import io.realm.Realm;

import static com.example.taeconsultant.finalproject.MyApplication.getApplication;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends BaseFragment implements IDetailMVPView {

    Realm realm;
    RealmController realmController;
    EventsDatabase eventsDatabase;

    IActivityComponent mIActivityComponent;

    @Inject
    DetailPresenter<IDetailMVPView> detailMVPViewDetailPresenter;

    String id = "";

    ArrayList<String> eventStrings = new ArrayList<>();

    @BindView(R.id.ivDetailIcon)
    ImageView ivDetailIcon;
    @BindView(R.id.tvDetailTitle)
    TextView tvDetailTitle;
    @BindView(R.id.tvDetailVenue1)
    TextView tvDetailVenue1;
    @BindView(R.id.tvDetailVenue2)
    TextView tvDetailVenue2;
    @BindView(R.id.tvDetailDatePrefix)
    TextView tvDetailDatePrefix;
    @BindView(R.id.tvDetailStartDate)
    TextView tvDetailStartDate;
    @BindView(R.id.tvDetailSaleDatePrefix)
    TextView tvDetailSaleDatePrefix;
    @BindView(R.id.tvDetailSaleStartDate)
    TextView tvDetailSaleStartDate;
    @BindView(R.id.tvDetailInfo)
    TextView tvDetailInfo;
    @BindView(R.id.fabMenu)
    FloatingActionMenu fabMenu;
    @BindView(R.id.fabMap)
    FloatingActionButton fabMap;
    @BindView(R.id.fabCalendar)
    FloatingActionButton fabCalendar;
    @BindView(R.id.fabTicket)
    FloatingActionButton fabTicket;

    public DetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        id = getArguments().getString("ID");

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeRealm();

        initializeViews(view);

        initializeDagger();

        detailMVPViewDetailPresenter.onAttach(this);
        if(((MainActivity)getActivity()).getConnectionState()=="DISCONNECTED") {
            Log.i("FINALPROJECT","No Connection, loading data from Realm");
            loadDataFromRealm(id);
            fabMap.setEnabled(false);
        } else {
            Log.i("FINALPROJECT","Connection, loading data from API");
            detailMVPViewDetailPresenter.onEventDetailViewPrepared(id);
        }
    }

    private void initializeRealm() {
        realm = Realm.getDefaultInstance();
        realmController = new RealmController(realm);
    }

    private void initializeDagger() {
        mIActivityComponent = DaggerIActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .iApplicationComponent(((MyApplication) getApplication()).getiApplicationComponent())
                .build();

        getIFragmentComponent().inject(this);
    }

    public void initializeViews(View view) {

        fabMap.setOnClickListener(view1 -> {
            MapFragment mapFragment = new MapFragment();

            mapFragment.setEnterTransition(new Slide(5));
            setExitTransition(new Slide(3));

            Bundle data = new Bundle();
            data.putDouble("UserLat",((MainActivity)getActivity()).userLocation.getLatitude());
            data.putDouble("UserLng",((MainActivity)getActivity()).userLocation.getLongitude());
            data.putStringArrayList("EventStrings",eventStrings);
            mapFragment.setArguments(data);

            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, mapFragment)
                    .addToBackStack(null)
                    .commit();
        });
    }

    @Override
    public void onFetchDataCompleted(final EventsModel.Embedded.Event event) {

        eventStrings.add(event.getName());
        eventStrings.add(event.getDates().getStart().getLocalDate());
        eventStrings.add(event.getEmbedded().getVenues().get(0).getName());
        eventStrings.add((event.getEmbedded().getVenues().get(0).getLocation()!=null)?event.getEmbedded().getVenues().get(0).getLocation().getLatitude():"");
        eventStrings.add((event.getEmbedded().getVenues().get(0).getLocation()!=null)?event.getEmbedded().getVenues().get(0).getLocation().getLongitude():"");
        eventStrings.add(event.getImages().get(0).getUrl());

        Picasso.with(getContext())
                .load(event.getImages().get(0).getUrl())
                .resize(ivDetailIcon.getWidth(),ivDetailIcon.getHeight())
                .centerCrop()
                .into(ivDetailIcon);

        fabTicket.setOnClickListener(view -> {
            if(((MainActivity)getActivity()).getConnectionState()=="CONNECTED") {
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(getContext(), Uri.parse(event.getUrl()));
            } else {
                //TODO: NO CONNECTION
            }
        });

        fabCalendar.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.Events.TITLE, event.getName())
                    .putExtra(CalendarContract.Events.EVENT_LOCATION, event.getEmbedded().getVenues().get(0).getAddress().getLine1())
                    .putExtra(CalendarContract.Events.EVENT_TIMEZONE, event.getDates().getTimezone())
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event.getDates().getStart().getLocalTime());

            startActivity(intent);
        });

        detailMVPViewDetailPresenter.onVenueDetailViewPrepared(event.getEmbedded().getVenues().get(0).getId());

        tvDetailTitle.setText(event.getName());
        tvDetailVenue1.setText((event.getEmbedded().getVenues().get(0).getName()!=null)?event.getEmbedded().getVenues().get(0).getName():getString(R.string.detail_no_info));

        String[] startDate = event.getDates().getStart().getLocalDate().split("-");

        tvDetailStartDate.setText(startDate[2] + "/" + startDate[1] + "/" + startDate[0]);

        String eventSalesPublicDateTime = event.getSales().getPublic().getStartDateTime();

        try {
            if(Calendar.getInstance().getTime().after(new SimpleDateFormat("yyyy-MM-dd").parse(eventSalesPublicDateTime.split("T")[0]))) {
                eventSalesPublicDateTime = event.getSales().getPublic().getEndDateTime();
                tvDetailSaleDatePrefix.setText(getString(R.string.detail_sale_end_date));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String saleTime = eventSalesPublicDateTime.split("T")[1];
        saleTime = saleTime.substring(0,saleTime.length()-2);
        String[] saleDate = eventSalesPublicDateTime.split("T")[0].split("-");

        tvDetailSaleStartDate.setText(saleDate[2] + "/" + saleDate[1] + "/" + saleDate[0] + " " + saleTime);
        tvDetailInfo.setText((event.getInfo()!=null)?event.getInfo():getString(R.string.detail_no_info));
    }

    @Override
    public void onFetchDataCompleted(VenuesModel.Embedded.Venue venue) {
        tvDetailVenue2.setText((venue.getAddress().getLine1()!=null)?venue.getAddress().getLine1():getString(R.string.detail_no_address));
    }

    @Override
    public void onError(String message) {
        super.onError(message);
        Log.i("FINALPROJECT","DetailFragment:onError:" + message);
    }

    public IActivityComponent getIFragmentComponent() {
        return mIActivityComponent;
    }

    public void loadDataFromRealm(String id) {

        eventsDatabase = realmController.getEventData(id);

        Bitmap bitmapPhoto = BitmapFactory.decodeByteArray(eventsDatabase.getIconLarge(),0,eventsDatabase.getIconLarge().length);
        ivDetailIcon.setImageBitmap(bitmapPhoto);

        tvDetailTitle.setText(eventsDatabase.getName());
        tvDetailVenue1.setText((eventsDatabase.getVenue()!="")?eventsDatabase.getVenue():getString(R.string.detail_no_info));
        tvDetailVenue2.setText((eventsDatabase.getVenueAddress()!="")?eventsDatabase.getVenueAddress():getString(R.string.detail_no_address));

        String[] startDate = eventsDatabase.getStartDate().split("-");
        tvDetailStartDate.setText(startDate[2] + "/" + startDate[1] + "/" + startDate[0]);

        String eventSalesPublicDateTime = eventsDatabase.getSaleStart();

        try {
            if(Calendar.getInstance().getTime().after(new SimpleDateFormat("yyyy-MM-dd").parse(eventSalesPublicDateTime.split("T")[0]))) {
                eventSalesPublicDateTime = eventsDatabase.getSaleEnd();
                tvDetailSaleDatePrefix.setText(getString(R.string.detail_sale_end_date));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String saleTime = eventSalesPublicDateTime.split("T")[1];
        saleTime = saleTime.substring(0,saleTime.length()-2);
        String[] saleDate = eventSalesPublicDateTime.split("T")[0].split("-");
        tvDetailSaleStartDate.setText(saleDate[2] + "/" + saleDate[1] + "/" + saleDate[0] + " " + saleTime);

        tvDetailInfo.setText((eventsDatabase.getInfo()!="")?eventsDatabase.getInfo():getString(R.string.detail_no_info));
        tvDetailInfo.setText(eventsDatabase.getInfo());
    }
}
