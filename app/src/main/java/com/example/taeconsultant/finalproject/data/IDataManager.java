package com.example.taeconsultant.finalproject.data;

import com.example.taeconsultant.finalproject.data.network.IAPIHelper;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public interface IDataManager extends IAPIHelper {
}
