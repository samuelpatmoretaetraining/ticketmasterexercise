package com.example.taeconsultant.finalproject.data.network.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by TAE Consultant on 23/10/2017.
 */

public class VenuesModel {

    @SerializedName("_embedded")
    @Expose
    private Embedded embedded;
    @SerializedName("_links")
    @Expose
    private Links_ links;
    @SerializedName("page")
    @Expose
    private Page page;

    public Embedded getEmbedded() {
        return embedded;
    }

    public Links_ getLinks() {
        return links;
    }

    public Page getPage() {
        return page;
    }

    public class Embedded {

        @SerializedName("venues")
        @Expose
        private List<Venue> venues = null;

        public List<Venue> getVenues() {
            return venues;
        }

        public class Venue {

            @SerializedName("genre")
            @Expose
            private String name;
            @SerializedName("type")
            @Expose
            private String type;
            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("test")
            @Expose
            private Boolean test;
            @SerializedName("url")
            @Expose
            private String url;
            @SerializedName("locale")
            @Expose
            private String locale;
            @SerializedName("postalCode")
            @Expose
            private String postalCode;
            @SerializedName("timezone")
            @Expose
            private String timezone;
            @SerializedName("city")
            @Expose
            private City city;
            @SerializedName("state")
            @Expose
            private State state;
            @SerializedName("country")
            @Expose
            private Country country;
            @SerializedName("address")
            @Expose
            private Address address;
            @SerializedName("location")
            @Expose
            private Location location;
            @SerializedName("markets")
            @Expose
            private List<Market> markets = null;
            @SerializedName("dmas")
            @Expose
            private List<Dma> dmas = null;
            @SerializedName("upcomingEvents")
            @Expose
            private UpcomingEvents upcomingEvents;
            @SerializedName("_links")
            @Expose
            private Links links;
            @SerializedName("images")
            @Expose
            private List<Image> images = null;
            @SerializedName("boxOfficeInfo")
            @Expose
            private BoxOfficeInfo boxOfficeInfo;
            @SerializedName("parkingDetail")
            @Expose
            private String parkingDetail;
            @SerializedName("accessibleSeatingDetail")
            @Expose
            private String accessibleSeatingDetail;
            @SerializedName("generalInfo")
            @Expose
            private GeneralInfo generalInfo;

            public String getName() {
                return name;
            }

            public String getType() {
                return type;
            }

            public String getId() {
                return id;
            }

            public Boolean getTest() {
                return test;
            }

            public String getUrl() {
                return url;
            }

            public String getLocale() {
                return locale;
            }

            public String getPostalCode() {
                return postalCode;
            }

            public String getTimezone() {
                return timezone;
            }

            public City getCity() {
                return city;
            }

            public State getState() {
                return state;
            }

            public Country getCountry() {
                return country;
            }

            public Address getAddress() {
                return address;
            }

            public Location getLocation() {
                return location;
            }

            public List<Market> getMarkets() {
                return markets;
            }

            public List<Dma> getDmas() {
                return dmas;
            }

            public UpcomingEvents getUpcomingEvents() {
                return upcomingEvents;
            }

            public Links getLinks() {
                return links;
            }

            public List<Image> getImages() {
                return images;
            }

            public BoxOfficeInfo getBoxOfficeInfo() {
                return boxOfficeInfo;
            }

            public String getParkingDetail() {
                return parkingDetail;
            }

            public String getAccessibleSeatingDetail() {
                return accessibleSeatingDetail;
            }

            public GeneralInfo getGeneralInfo() {
                return generalInfo;
            }

            public class Address {

                @SerializedName("line1")
                @Expose
                private String line1;

                public String getLine1() {
                    return line1;
                }

            }

            public class BoxOfficeInfo {

                @SerializedName("phoneNumberDetail")
                @Expose
                private String phoneNumberDetail;
                @SerializedName("openHoursDetail")
                @Expose
                private String openHoursDetail;
                @SerializedName("acceptedPaymentDetail")
                @Expose
                private String acceptedPaymentDetail;
                @SerializedName("willCallDetail")
                @Expose
                private String willCallDetail;

                public String getPhoneNumberDetail() {
                    return phoneNumberDetail;
                }

                public String getOpenHoursDetail() {
                    return openHoursDetail;
                }

                public String getAcceptedPaymentDetail() {
                    return acceptedPaymentDetail;
                }

                public String getWillCallDetail() {
                    return willCallDetail;
                }

            }

            public class City {

                @SerializedName("genre")
                @Expose
                private String name;

                public String getName() {
                    return name;
                }

            }

            public class Country {

                @SerializedName("genre")
                @Expose
                private String name;
                @SerializedName("countryCode")
                @Expose
                private String countryCode;

                public String getName() {
                    return name;
                }

                public String getCountryCode() {
                    return countryCode;
                }

            }

            public class Dma {

                @SerializedName("id")
                @Expose
                private Integer id;

                public Integer getId() {
                    return id;
                }

            }

            public class GeneralInfo {

                @SerializedName("generalRule")
                @Expose
                private String generalRule;
                @SerializedName("childRule")
                @Expose
                private String childRule;

                public String getGeneralRule() {
                    return generalRule;
                }

                public String getChildRule() {
                    return childRule;
                }

            }

            public class Image {

                @SerializedName("ratio")
                @Expose
                private String ratio;
                @SerializedName("url")
                @Expose
                private String url;
                @SerializedName("width")
                @Expose
                private Integer width;
                @SerializedName("height")
                @Expose
                private Integer height;
                @SerializedName("fallback")
                @Expose
                private Boolean fallback;

                public String getRatio() {
                    return ratio;
                }

                public String getUrl() {
                    return url;
                }

                public Integer getWidth() {
                    return width;
                }

                public Integer getHeight() {
                    return height;
                }

                public Boolean getFallback() {
                    return fallback;
                }

            }

            public class Links {

                @SerializedName("self")
                @Expose
                private Self self;

                public Self getSelf() {
                    return self;
                }

                public class Self {

                    @SerializedName("href")
                    @Expose
                    private String href;

                    public String getHref() {
                        return href;
                    }

                }
            }

            public class Location {

                @SerializedName("longitude")
                @Expose
                private String longitude;
                @SerializedName("latitude")
                @Expose
                private String latitude;

                public String getLongitude() {
                    return longitude;
                }

                public String getLatitude() {
                    return latitude;
                }

            }

            public class Market {

                @SerializedName("id")
                @Expose
                private String id;

                public String getId() {
                    return id;
                }

            }

            public class State {

                @SerializedName("genre")
                @Expose
                private String name;
                @SerializedName("stateCode")
                @Expose
                private String stateCode;

                public String getName() {
                    return name;
                }

                public String getStateCode() {
                    return stateCode;
                }

            }

            public class UpcomingEvents {

                @SerializedName("_total")
                @Expose
                private Integer total;

                public Integer getTotal() {
                    return total;
                }

            }
        }
    }

    public class Links_ {

        @SerializedName("first")
        @Expose
        private First first;
        @SerializedName("self")
        @Expose
        private Self_ self;
        @SerializedName("next")
        @Expose
        private Next next;
        @SerializedName("last")
        @Expose
        private Last last;

        public First getFirst() {
            return first;
        }

        public Self_ getSelf() {
            return self;
        }

        public Next getNext() {
            return next;
        }

        public Last getLast() {
            return last;
        }

        public class First {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }

        public class Last {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }

        public class Next {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }

        public class Self_ {

            @SerializedName("href")
            @Expose
            private String href;

            public String getHref() {
                return href;
            }

        }
    }

    public class Page {

        @SerializedName("size")
        @Expose
        private Integer size;
        @SerializedName("totalElements")
        @Expose
        private Integer totalElements;
        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("number")
        @Expose
        private Integer number;

        public Integer getSize() {
            return size;
        }

        public Integer getTotalElements() {
            return totalElements;
        }

        public Integer getTotalPages() {
            return totalPages;
        }

        public Integer getNumber() {
            return number;
        }

    }
}