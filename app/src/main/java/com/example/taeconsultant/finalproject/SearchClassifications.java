package com.example.taeconsultant.finalproject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samuelpatmore on 29/01/2018.
 */

public class SearchClassifications {

    public static List<Classification> getBackupClassifications() {
        List<Classification> classifications = new ArrayList<>();

        classifications.add(new Classification("Music", "Music", "KZFzniwnSyZfZ7v7nJ"));
        classifications.add(new Classification("Music", "Alternative", "KnvZfZ7vAvv"));
        classifications.add(new Classification("Music", "Ballads/Romantic", "KnvZfZ7vAve"));
        classifications.add(new Classification("Music", "Blues", "KnvZfZ7vAvd"));
        classifications.add(new Classification("Music", "Chanson Francaise", "KnvZfZ7vAvA"));
        classifications.add(new Classification("Music", "Children's Music", "KnvZfZ7vAvk"));
        classifications.add(new Classification("Music", "Classical", "KnvZfZ7vAeJ"));
        classifications.add(new Classification("Music", "Country", "KnvZfZ7vAv6"));
        classifications.add(new Classification("Music", "Dance/Electronic", "KnvZfZ7vAvF"));
        classifications.add(new Classification("Music", "Folk", "KnvZfZ7vAva"));
        classifications.add(new Classification("Music", "Hip-Hop/Rap", "KnvZfZ7vAv1"));
        classifications.add(new Classification("Music", "Holiday", "KnvZfZ7vAvJ"));
        classifications.add(new Classification("Music", "Jazz", "KnvZfZ7vAvE"));
        classifications.add(new Classification("Music", "Medieval/Renaissance", "KnvZfZ7vAvI"));
        classifications.add(new Classification("Music", "Metal", "KnvZfZ7vAvt"));
        classifications.add(new Classification("Music", "New Age", "KnvZfZ7vAvn"));
        classifications.add(new Classification("Music", "Other", "KnvZfZ7vAvl"));
        classifications.add(new Classification("Music", "Pop", "KnvZfZ7vAev"));
        classifications.add(new Classification("Music", "R&B", "KnvZfZ7vAee"));
        classifications.add(new Classification("Music", "Reggae", "KnvZfZ7vAed"));
        classifications.add(new Classification("Music", "Religious", "KnvZfZ7vAe7"));
        classifications.add(new Classification("Music", "Rock", "KnvZfZ7vAeA"));
        classifications.add(new Classification("Music", "Undefined", "KnvZfZ7vAe6"));
        classifications.add(new Classification("Music", "World", "KnvZfZ7vAeF"));
        classifications.add(new Classification("Film", "Film", "KZFzniwnSyZfZ7v7nn"));
        classifications.add(new Classification("Film", "Action/Adventure", "KnvZfZ7vAke"));
        classifications.add(new Classification("Film", "Animation", "KnvZfZ7vAkd"));
        classifications.add(new Classification("Film", "Arthouse", "KnvZfZ7vAk7"));
        classifications.add(new Classification("Film", "Comedy", "KnvZfZ7vAkA"));
        classifications.add(new Classification("Film", "Documentary", "KnvZfZ7vAkk"));
        classifications.add(new Classification("Film", "Drama", "KnvZfZ7vAk6"));
        classifications.add(new Classification("Film", "Family", "KnvZfZ7vAkF"));
        classifications.add(new Classification("Film", "Foreign", "KnvZfZ7vAk1"));
        classifications.add(new Classification("Film", "Miscellaneous", "KnvZfZ7vAka"));
        classifications.add(new Classification("Film", "Music", "KnvZfZ7vAkJ"));
        classifications.add(new Classification("Film", "Urban", "KnvZfZ7vAkE"));
        classifications.add(new Classification("Sports", "Sports", "KZFzniwnSyZfZ7v7nE"));
        classifications.add(new Classification("Sports", "Aquatics", "KnvZfZ7vAeI"));
        classifications.add(new Classification("Sports", "Athletic Races", "KnvZfZ7vAet"));
        classifications.add(new Classification("Sports", "Badminton", "KnvZfZ7vAen"));
        classifications.add(new Classification("Sports", "Bandy", "KnvZfZ7vAel"));
        classifications.add(new Classification("Sports", "Baseball", "KnvZfZ7vAdv"));
        classifications.add(new Classification("Sports", "Basketball", "KnvZfZ7vAde"));
        classifications.add(new Classification("Sports", "Biathlon", "KnvZfZ7vAdd"));
        classifications.add(new Classification("Sports", "Body Building", "KnvZfZ7vAd7"));
        classifications.add(new Classification("Sports", "Boxing", "KnvZfZ7vAdA"));
        classifications.add(new Classification("Sports", "Cricket", "KnvZfZ7vAdk"));
        classifications.add(new Classification("Sports", "Curling", "KnvZfZ7vAdF"));
        classifications.add(new Classification("Sports", "Cycling", "KnvZfZ7vAda"));
        classifications.add(new Classification("Sports", "Equestrian", "KnvZfZ7vAd1"));
        classifications.add(new Classification("Sports", "Extreme", "KnvZfZ7vAdJ"));
        classifications.add(new Classification("Sports", "Floorball", "KnvZfZ7vA1l"));
        classifications.add(new Classification("Sports", "Football", "KnvZfZ7vAdE"));
        classifications.add(new Classification("Sports", "Golf", "KnvZfZ7vAdt"));
        classifications.add(new Classification("Sports", "Gymnastics", "KnvZfZ7vAdn"));
        classifications.add(new Classification("Sports", "Handball", "KnvZfZ7vAdl"));
        classifications.add(new Classification("Sports", "Hockey", "KnvZfZ7vAdI"));
        classifications.add(new Classification("Sports", "Ice Skating", "KnvZfZ7vA7v"));
        classifications.add(new Classification("Sports", "Indoor Soccer", "KnvZfZ7vA7e"));
        classifications.add(new Classification("Sports", "Lacrosse", "KnvZfZ7vA77"));
        classifications.add(new Classification("Sports", "Martial Arts", "KnvZfZ7vA7d"));
        classifications.add(new Classification("Sports", "Miscellaneous", "KnvZfZ7vA7A"));
        classifications.add(new Classification("Sports", "Motorsports/Racing", "KnvZfZ7vA7k"));
        classifications.add(new Classification("Sports", "Netball", "KnvZfZ7vA76"));
        classifications.add(new Classification("Sports", "Rodeo", "KnvZfZ7vAea"));
        classifications.add(new Classification("Sports", "Roller Hockey", "KnvZfZ7vA7a"));
        classifications.add(new Classification("Sports", "Rugby", "KnvZfZ7vA71"));
        classifications.add(new Classification("Sports", "Ski Jumping", "KnvZfZ7vA7J"));
        classifications.add(new Classification("Sports", "Skiing", "KnvZfZ7vAd6"));
        classifications.add(new Classification("Sports", "Soccer", "KnvZfZ7vA7E"));
        classifications.add(new Classification("Sports", "Squash", "KnvZfZ7vA7I"));
        classifications.add(new Classification("Sports", "Surfing", "KnvZfZ7vA7t"));
        classifications.add(new Classification("Sports", "Swimming", "KnvZfZ7vA7n"));
        classifications.add(new Classification("Sports", "Table Tennis", "KnvZfZ7vA7l"));
        classifications.add(new Classification("Sports", "Tennis", "KnvZfZ7vAAv"));
        classifications.add(new Classification("Sports", "Toros", "KnvZfZ7vAAe"));
        classifications.add(new Classification("Sports", "Track & Field", "KnvZfZ7vAAd"));
        classifications.add(new Classification("Sports", "Volleyball", "KnvZfZ7vAA7"));
        classifications.add(new Classification("Sports", "Waterpolo", "KnvZfZ7vAAA"));
        classifications.add(new Classification("Sports", "Wrestling", "KnvZfZ7vAAk"));
        classifications.add(new Classification("Arts & Theatre", "Arts & Theatre", "KZFzniwnSyZfZ7v7na"));
        classifications.add(new Classification("Arts & Theatre", "Children's Theatre", "KnvZfZ7v7na"));
        classifications.add(new Classification("Arts & Theatre", "Circus & Specialty Acts", "KnvZfZ7v7n1"));
        classifications.add(new Classification("Arts & Theatre", "Classical", "KnvZfZ7v7nJ"));
        classifications.add(new Classification("Arts & Theatre", "Comedy", "KnvZfZ7vAe1"));
        classifications.add(new Classification("Arts & Theatre", "Cultural", "KnvZfZ7v7nE"));
        classifications.add(new Classification("Arts & Theatre", "Dance", "KnvZfZ7v7nI"));
        classifications.add(new Classification("Arts & Theatre", "Espectaculo", "KnvZfZ7v7nt"));
        classifications.add(new Classification("Arts & Theatre", "Fashion", "KnvZfZ7v7nn"));
        classifications.add(new Classification("Arts & Theatre", "Fine Art", "KnvZfZ7v7nl"));
        classifications.add(new Classification("Arts & Theatre", "Magic & Illusion", "KnvZfZ7v7lv"));
        classifications.add(new Classification("Arts & Theatre", "Miscellaneous", "KnvZfZ7v7le"));
        classifications.add(new Classification("Arts & Theatre", "Miscellaneous Theatre", "KnvZfZ7v7ld"));
        classifications.add(new Classification("Arts & Theatre", "Multimedia", "KnvZfZ7v7l7"));
        classifications.add(new Classification("Arts & Theatre", "Music", "KnvZfZ7v7lA"));
        classifications.add(new Classification("Arts & Theatre", "Opera", "KnvZfZ7v7lk"));
        classifications.add(new Classification("Arts & Theatre", "Performance Art", "KnvZfZ7v7l6"));
        classifications.add(new Classification("Arts & Theatre", "Puppetry", "KnvZfZ7v7lF"));
        classifications.add(new Classification("Arts & Theatre", "Spectacular", "KnvZfZ7v7la"));
        classifications.add(new Classification("Arts & Theatre", "Theatre", "KnvZfZ7v7l1"));
        classifications.add(new Classification("Arts & Theatre", "Variety", "KnvZfZ7v7lJ"));
        
        return classifications;
    }
}
