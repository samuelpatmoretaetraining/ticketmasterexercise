package com.example.taeconsultant.finalproject;

import android.support.annotation.NonNull;

import com.yalantis.filter.model.FilterModel;

/**
 * Created by TAE Consultant on 01/11/2017.
 */

public class Tag implements FilterModel{
    private String text;
    private int colour;

    public Tag(String text, int colour) {
        this.text = text;
        this.colour = colour;
    }

    @NonNull
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getColour() {
        return colour;
    }

    public void setColour(int colour) {
        this.colour = colour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;

        Tag tag = (Tag) o;

        if (getColour() != tag.getColour()) return false;
        return getText().equals(tag.getText());

    }

    @Override
    public int hashCode() {
        int result = getText().hashCode();
        result = 31 * result + getColour();
        return result;
    }
}
