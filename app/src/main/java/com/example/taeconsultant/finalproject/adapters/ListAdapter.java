package com.example.taeconsultant.finalproject.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.taeconsultant.finalproject.utility.CircleTransform;
import com.example.taeconsultant.finalproject.ListFragment;
import com.example.taeconsultant.finalproject.R;
import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {

    EventsModel eventsModel;
    int row;
    Context applicationContext;
    private final ListFragment.OnItemClickListener listener;

    public ListAdapter(EventsModel eventsModel, int row, Context applicationContext, ListFragment.OnItemClickListener listener) {
        this.eventsModel = eventsModel;
        this.row = row;
        this.applicationContext = applicationContext;
        this.listener = listener;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(row,parent,false);
        return new ListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {

        holder.bind(eventsModel.getEmbedded().getEvents().get(position).getId(), holder, listener);

        Picasso.with(applicationContext)
                .load(eventsModel.getEmbedded().getEvents().get(position).getImages().get(0).getUrl())
                .resize(100,100)
                .centerCrop()
                .transform(new CircleTransform())
                .into(holder.ivIcon);

        holder.tvName.setText(eventsModel.getEmbedded().getEvents().get(position).getName());
        try {
            holder.tvVenue.setText(eventsModel.getEmbedded().getEvents().get(position).getEmbedded().getVenues().get(0).getName());
        } catch (NullPointerException e) {
//            e.printStackTrace();
            Log.w("ListAdapter onBindVH", e.getMessage(), e);
        }
        String[] date = eventsModel.getEmbedded().getEvents().get(position).getDates().getStart().getLocalDate().split("-");
        holder.tvDate.setText(date[2] + "/" + date[1] + "/" + date[0]);
    }

    @Override
    public int getItemCount() {

        return eventsModel.getEmbedded().getEvents().size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.ivRowIcon)
        public ImageView ivIcon;
        @BindView(R.id.tvRowName)
        public TextView tvName;
        @BindView(R.id.tvRowVenue)
        public TextView tvVenue;
        @BindView(R.id.tvRowDate)
        public TextView tvDate;
        @BindView(R.id.cvRowCard)
        public CardView cvRowCard;

        public ListViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bind(final String id, final ListViewHolder holder, final ListFragment.OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(holder, id));
        }
    }
}
