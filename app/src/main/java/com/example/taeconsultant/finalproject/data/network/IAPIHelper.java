package com.example.taeconsultant.finalproject.data.network;

import com.example.taeconsultant.finalproject.data.network.models.ClassificationsModel;
import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.example.taeconsultant.finalproject.data.network.models.VenuesModel;

import io.reactivex.Observable;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public interface IAPIHelper {

    /**
     *
     * @param apikey
     * The API Key
     * @param keyword
     * Keyword to search on
     * @param latlong
     * Filter events by latitude and longitude, this filter is deprecated and maybe removed in a future release, please use geoPoint instead
     * @param radius
     * Radius of the area in which we want to search for events.
     * @param unit
     * Unit of the radius
     * Default: miles
     * @param size
     * Page size of the response
     * Default: 20
     * @param page
     * Page number
     * Default: 0
     * @param sort
     * Sorting order of the search result. Allowable values : 'genre,asc', 'genre,desc', 'date,asc', 'date,desc', 'relevance,asc', 'relevance,desc', 'distance,asc', 'genre,date,asc', 'genre,date,desc', 'date,genre,asc', 'date,genre,desc','onsaleStartDate,asc', 'id,asc'
     * Default: relevance,desc
     * @param countryCode
     * Filter events by country code
     * @param classificationId
     * Filter events by classification id: id of any segment, genre, sub-genre, type, sub-type. Negative filtering is supported by using the following format '-'. Be aware that negative filters may cause decreased performance
     * @param includeLicensedContent
     * Yes if you want to display licensed content
     * Default: no
     * @return
     */
    Observable<EventsModel> useCaseEvents(
            String apikey,
            String keyword,
            String latlong,
            int radius,
            String unit,
            int size,
            int page,
            String sort,
            String countryCode,
            String classificationId,
            String includeLicensedContent);

    Observable<EventsModel.Embedded.Event> useCaseEventDetails(String id, String apikey);

    Observable<ClassificationsModel> useCaseClassifications(String apikey);

    Observable<VenuesModel.Embedded.Venue> useCaseVenueDetails(String id, String apikey);
}
