package com.example.taeconsultant.finalproject.data.database.localdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by TAE Consultant on 26/10/2017.
 */

public class EventsDatabase extends RealmObject {

    @PrimaryKey
    String id;
    String name;
    String latititude;
    String longitude;
    String startDate;
    String venue;
    String venueAddress;
    String saleStart;
    String saleEnd;
    String info;
    byte[] iconSmall;
    byte[] iconLarge;

    /**
     *
     * @param id
     * Primary Key
     * @param name
     * @param latititude
     * @param longitude
     * @param startDate
     * @param venue
     * @param venueAddress
     * @param saleStart
     * @param saleEnd
     * @param info
     * @param iconSmall
     * @param iconLarge
     */
    public EventsDatabase(
            String id,
            String name,
            String latititude,
            String longitude,
            String startDate,
            String venue,
            String venueAddress,
            String saleStart,
            String saleEnd,
            String info,
            byte[] iconSmall,
            byte[] iconLarge) {

        this.id = id;
        this.name = name;
        this.latititude = latititude;
        this.longitude = longitude;
        this.startDate = startDate;
        this.venue = venue;
        this.venueAddress = venueAddress;
        this.saleStart = saleStart;
        this.saleEnd = saleEnd;
        this.info = info;
        this.iconSmall = iconSmall;
        this.iconLarge = iconLarge;
    }

    public EventsDatabase() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatititude() {
        return latititude;
    }

    public void setLatititude(String latititude) {
        this.latititude = latititude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getVenueAddress() {
        return venueAddress;
    }

    public void setVenueAddress(String venueAddress) {
        this.venueAddress = venueAddress;
    }

    public String getSaleStart() {
        return saleStart;
    }

    public void setSaleStart(String saleStart) {
        this.saleStart = saleStart;
    }

    public String getSaleEnd() {
        return saleEnd;
    }

    public void setSaleEnd(String saleEnd) {
        this.saleEnd = saleEnd;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public byte[] getIconSmall() {
        return iconSmall;
    }

    public void setIconSmall(byte[] iconSmall) {
        this.iconSmall = iconSmall;
    }

    public byte[] getIconLarge() {
        return iconLarge;
    }

    public void setIconLarge(byte[] iconLarge) {
        this.iconLarge = iconLarge;
    }
}
