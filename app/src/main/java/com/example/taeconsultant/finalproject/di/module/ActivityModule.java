package com.example.taeconsultant.finalproject.di.module;

import android.content.Context;

import com.example.taeconsultant.finalproject.di.scope.ActivityContext;
import com.example.taeconsultant.finalproject.ui.base.BaseFragment;
import com.example.taeconsultant.finalproject.ui.detailfragment.DetailPresenter;
import com.example.taeconsultant.finalproject.ui.detailfragment.IDetailMVPPresenter;
import com.example.taeconsultant.finalproject.ui.detailfragment.IDetailMVPView;
import com.example.taeconsultant.finalproject.ui.listfragment.IListMVPPresenter;
import com.example.taeconsultant.finalproject.ui.listfragment.IListMVPView;
import com.example.taeconsultant.finalproject.ui.listfragment.ListPresenter;
import com.example.taeconsultant.finalproject.utility.rx.AppSchedulerProvider;
import com.example.taeconsultant.finalproject.utility.rx.SchedulerProvider;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by TAE Consultant on 03/10/2017.
 */

@Module
public class ActivityModule {

    BaseFragment baseFragment;

    public ActivityModule(BaseFragment baseFragment) {
        this.baseFragment = baseFragment;
    }

    @Provides
    @ActivityContext
    Context getContext() {
        return baseFragment.getContext();
    }

    @Provides
    BaseFragment getAppCompatActivity() {
        return baseFragment;
    }

    @Provides
    CompositeDisposable compositeDisposable(){
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider schedulerProvider() {
        return new AppSchedulerProvider();
    }

    /**
     * Presenter object
     */

    @Provides
    IListMVPPresenter<IListMVPView> iListMVPViewIListMVPPresenter(ListPresenter<IListMVPView> listMVPViewListPresenter) {
        return listMVPViewListPresenter;
    }

    @Provides
    IDetailMVPPresenter<IDetailMVPView> iDetailMVPViewIDetailMVPPresenter(DetailPresenter<IDetailMVPView> detailMVPViewDetailPresenter) {
        return detailMVPViewDetailPresenter;
    }
}
