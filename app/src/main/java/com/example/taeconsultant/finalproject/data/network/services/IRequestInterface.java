package com.example.taeconsultant.finalproject.data.network.services;

import com.example.taeconsultant.finalproject.data.network.models.ClassificationsModel;
import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.example.taeconsultant.finalproject.data.network.models.VenuesModel;
import com.example.taeconsultant.finalproject.utility.Constants;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IRequestInterface {

    /**
     *
     * @param apikey
     * The API Key
     * @param keyword
     * Keyword to search on
     * @param latlong
     * Filter events by latitude and longitude, this filter is deprecated and maybe removed in a future release, please use geoPoint instead
     * @param radius
     * Radius of the area in which we want to search for events.
     * @param unit
     * Unit of the radius
     * Default: miles
     * @param size
     * Page size of the response
     * Default: 20
     * @param page
     * Page number
     * Default: 0
     * @param sort
     * Sorting order of the search result. Allowable values : 'genre,asc', 'genre,desc', 'date,asc', 'date,desc', 'relevance,asc', 'relevance,desc', 'distance,asc', 'genre,date,asc', 'genre,date,desc', 'date,genre,asc', 'date,genre,desc','onsaleStartDate,asc', 'id,asc'
     * Default: relevance,desc
     * @param countryCode
     * Filter events by country code
     * @param classificationId
     * Filter events by classification id: id of any segment, genre, sub-genre, type, sub-type. Negative filtering is supported by using the following format '-'. Be aware that negative filters may cause decreased performance
     * @param includeLicensedContent
     * Yes if you want to display licensed content
     * Default: no
     * @return
     */
    @GET(Constants.EVENTS_URL)
    Observable<EventsModel> getEvents(
            @Query("apikey") String apikey,
            @Query("keyword") String keyword,
            @Query("latlong") String latlong,
            @Query("radius") int radius,
            @Query("unit") String unit,
            @Query("size") int size,
            @Query("page") int page,
            @Query("sort") String sort,
            @Query("countryCode") String countryCode,
            @Query("classificationId") String classificationId,
            @Query("includeLicensedContent") String includeLicensedContent);

    @GET(Constants.EVENT_DETAIL_URL)
    Observable<EventsModel.Embedded.Event> getEventDetails(@Path("id") String id, @Query("apikey") String apikey);

    @GET(Constants.CLASSIFICATIONS_URL)
    Observable<ClassificationsModel> getClassifications(@Query("apikey") String apikey);

    @GET(Constants.VENUE_DETAIL_URL)
    Observable<VenuesModel.Embedded.Venue> getVenueDetails(@Path("id") String id, @Query("apikey") String apikey);
}