package com.example.taeconsultant.finalproject.data.network;

import com.example.taeconsultant.finalproject.data.network.models.ClassificationsModel;
import com.example.taeconsultant.finalproject.data.network.models.EventsModel;
import com.example.taeconsultant.finalproject.data.network.models.VenuesModel;
import com.example.taeconsultant.finalproject.data.network.services.ConnectionService;
import com.example.taeconsultant.finalproject.data.network.services.IRequestInterface;

import io.reactivex.Observable;

/**
 * Created by TAE Consultant on 18/10/2017.
 */

public class AppAPIHelper implements IAPIHelper{

    private IRequestInterface requestInterface;

    public AppAPIHelper() {
        this.requestInterface = ConnectionService.getConnectionService();
    }

    @Override
    public Observable<EventsModel> useCaseEvents(
            String apikey,
            String keyword,
            String latlong,
            int radius,
            String unit,
            int size,
            int page,
            String sort,
            String countryCode,
            String classificationId,
            String includeLicensedContent) {

        page = Math.max(page,0);
        size = Math.max(size,1);
        if(!SortParam.has(sort)) { sort = SortParam.DATE_DESC.text; }
        return requestInterface.getEvents(apikey, keyword, latlong, radius, unit, size, page, sort, countryCode, classificationId, includeLicensedContent);
    }

    @Override
    public Observable<EventsModel.Embedded.Event> useCaseEventDetails(String id, String apikey) {
        return requestInterface.getEventDetails(id, apikey);
    }

    @Override
    public Observable<ClassificationsModel> useCaseClassifications(String apikey) {
        return requestInterface.getClassifications(apikey);
    }

    @Override
    public Observable<VenuesModel.Embedded.Venue> useCaseVenueDetails(String id, String apikey) {
        return requestInterface.getVenueDetails(id, apikey);
    }

    public enum SortParam {
        NAME_ASC("genre,asc"),
        NAME_DESC("genre,desc"),
        DATE_ASC("date,asc"),
        DATE_DESC("date,desc"),
        RELEVANCE_ASC("relevance,asc"),
        RELEVANCE_DESC("relevance,desc"),
        DISTANCE_ASC("distance,asc"),
        DISTANCE_DESC("distance,desc"),
        ONSALESTARTDATE_ASC("onsaleStartDate,asc"),
        ONSALESTARTDATE_DESC("onsaleStartDate,desc");

        private final String text;

        private SortParam(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }

        public static boolean has(String s) {
            for(int i=0;i<SortParam.values().length;i++) {
                if(SortParam.values()[i].text == s) {
                    return true;
                }
            }
            return false;
        }
    }
}
